package com.example.cellinfo;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityNr;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoNr;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.Series;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Author: Jan Plachy (plachyjan@gmail.com)
 */
public class PhoneCallback extends PhoneStateListener {

    //--------------------------------------------------
    // Constants
    //--------------------------------------------------

    public static final String LOG_TAG = "PhoneCallback";
    private static final String MEASUREMENT_HEADER = "Technology Cell_Id MCC MNC PCI TAC  RSRP RSRP_SS Time Registered?\n";
    private static final String DEVICE_NAME_SM_T805 = "Samsung SM-T805";
    private static final String[] TECHNOLOGY_STRING_ARRAY = {"GSM", "CDMA", "WCDMA", "LTE", "NR"};
    private static final int SERIES_ACTIVITY_RESET_VALUE = 100;
    private static final long CELL_INFO_OUTDATED_THRESHOLD = 100000000;
    private static final int CELL_GRAPH_MIN_SIGNAL_LEVEL = -120;
    private static final String colorArray[] = {"#00FF00", "#0000FF", "#FF0000", "#01FFFE", "#FFA6FE", "#FFDB66", "#006401", "#010067", "#95003A", "#007DB5"};
    //--------------------------------------------------
    // Attributes
    //--------------------------------------------------
    TcpClient mTcpClient;
    private final TextView mTextView;
    private int technologyShowFilter = 0;
    private int rsrp;
    private String rsrpSignalStrength;
    private int cellId;
    private String Mcc;
    private String Mnc;
    private int Pci;
    private int Tac;
    private String technology;
    private long timeStamp;
    private long timeStampStartMeasuring;
    private boolean mMeasureOn = false;
    private boolean isRegistered;
    private String measOutput = "";
    private String mSignalStrength = "";
    private Context mContext;
    private String mFilename = "";
    private String mStoragePath = "";
    private String mStorageDirectory = "CellInfoData";
    private Object CellSignalStrengthLte;
    private String mDeviceModel;
    private Spinner mSpinnerFilter;
    private boolean mTechnologyFilter = false;
    private GraphView mGraph;
    private int mSwitchOption = 0;
    private int mSwitchLocalEnb = 0;
    private boolean mSwitchEnabled = true;
    private int measurementId = 0;
    private int ongoingMeasurementId = 0;
    private boolean mSeriesFull = false;
    private boolean localMeasurement = true;

    private String SERVER_IP = "";
    private int SERVER_PORT = 1324;

    // when there is time change it to dynamic creation of series and plot
    private LineGraphSeries<DataPoint> mSeries0 = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    private LineGraphSeries<DataPoint> mSeries1 = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    ; // = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    private LineGraphSeries<DataPoint> mSeries2 = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    ;
    private LineGraphSeries<DataPoint> mSeries3 = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    ;
    private LineGraphSeries<DataPoint> mSeries4 = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    ;
    private LineGraphSeries<DataPoint> mSeries5 = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    ;
    private LineGraphSeries<DataPoint> mSeries6 = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    ;
    private LineGraphSeries<DataPoint> mSeries7 = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    ;
    private LineGraphSeries<DataPoint> mSeries8 = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    ;
    private LineGraphSeries<DataPoint> mSeries9 = new LineGraphSeries<DataPoint>(new DataPoint[]{});
    ;

    private double graph2LastXValue = 5d;

    private List<long[]> mCellInfolist = new ArrayList<long[]>();
    private List<int[]> mGraphSeriesInfo = new ArrayList<int[]>();
    private List<Integer> mSeriesFlagged = new ArrayList<>();
    private double seriesLastValue[] = new double[10];
    private int seriesActivity[] = new int[10];
    private int mSeriesStatus[] = new int[10];

    private double cellLastDataIndex = 1d;


    //--------------------------------------------------
    // Constructor
    //--------------------------------------------------

    public PhoneCallback(TextView textView, Context context, String filename, String devicemodel, Spinner spinner, GraphView graph) {
        mTextView = textView;
        mContext = context;
        mFilename = filename + "input.txt";
        mDeviceModel = devicemodel;

        mGraph = graph;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            mStoragePath = mContext.getFilesDir().toString();
            //mStorageDirectory = "Documents";
           // mStoragePath = Environment.getExternalStorageDirectory().getPath();

        }else{
            mStoragePath = Environment.getExternalStorageDirectory().getPath();

        }

        File file = new File(mStoragePath, mStorageDirectory); //
        File gpxfile = new File(file, mFilename);

        if (!file.exists()) {
            file.mkdirs();
        }

        if (!gpxfile.exists()) {
            writeFileOnInternalStorage(mContext, mFilename, MEASUREMENT_HEADER, false);
        }
        initSeries();
        mSpinnerFilter = spinner;
        timeStampStartMeasuring =  System.currentTimeMillis();
    }

    //--------------------------------------------------
    // Methods
    //--------------------------------------------------

    private String serviceStateToString(int serviceState) {
        switch (serviceState) {
            case ServiceState.STATE_IN_SERVICE:
                return "STATE_IN_SERVICE";
            case ServiceState.STATE_OUT_OF_SERVICE:
                return "STATE_OUT_OF_SERVICE";
            case ServiceState.STATE_EMERGENCY_ONLY:
                return "STATE_EMERGENCY_ONLY";
            case ServiceState.STATE_POWER_OFF:
                return "STATE_POWER_OFF";
            default:
                return "UNKNOWN_STATE";
        }
    }

    private String callStateToString(int state) {
        switch (state) {
            case TelephonyManager.CALL_STATE_IDLE:
                return "\nonCallStateChanged: CALL_STATE_IDLE, ";
            case TelephonyManager.CALL_STATE_RINGING:
                return "\nonCallStateChanged: CALL_STATE_RINGING, ";
            case TelephonyManager.CALL_STATE_OFFHOOK:
                return "\nonCallStateChanged: CALL_STATE_OFFHOOK, ";
            default:
                return "\nUNKNOWN_STATE: " + state + ", ";
        }
    }

    //--------------------------------------------------
    // PhoneStateListener
    //--------------------------------------------------


    @Override
    public void onCellInfoChanged(List<CellInfo> cellInfoList) {
        super.onCellInfoChanged(cellInfoList);

        getCellInfoSignal(mContext);

        // Log.i(LOG_TAG, "onCellInfoChanged: " + cellInfo);
    }

    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);

        Log.i(LOG_TAG, "onSignalStrengthsChanged: SIGNAL_STRENGTH_CHANGE");
        String rsrpSignalStrengthString = signalStrength.toString();
        parseSignalStrength(rsrpSignalStrengthString);


        /*
        String[] rsrpSignalStrengthArr = rsrpSignalStrengthString.split(" ");
        if(rsrpSignalStrengthString.contains("CellSignalStrengthLte:")){
            String rsrpSignalStrengthTmp = rsrpSignalStrengthArr[2];
            String[] rsrpSignalStrengthAr = rsrpSignalStrengthTmp.split("=");
            rsrpSignalStrength = rsrpSignalStrengthAr[1];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                List<CellSignalStrength> cellSignalStrengths = signalStrength.getCellSignalStrengths();
                for (CellSignalStrength cellSignalStrength : cellSignalStrengths) {
                    if(cellSignalStrength.equals(CellSignalStrengthLte)){
                        rsrpSignalStrength= String.valueOf(cellSignalStrength.getDbm());
                    }
                }
            }

        }else{
            rsrpSignalStrength = rsrpSignalStrengthArr[9];

        }

         */
        getCellInfoSignal(mContext);
    }

    @Override
    public void onDataActivity(int direction) {
        super.onDataActivity(direction);
        getCellInfoSignal(mContext);
        switch (direction) {
            case TelephonyManager.DATA_ACTIVITY_NONE:
                Log.i(LOG_TAG, "onDataActivity: DATA_ACTIVITY_NONE");
                break;
            case TelephonyManager.DATA_ACTIVITY_IN:
                Log.i(LOG_TAG, "onDataActivity: DATA_ACTIVITY_IN");
                break;
            case TelephonyManager.DATA_ACTIVITY_OUT:
                Log.i(LOG_TAG, "onDataActivity: DATA_ACTIVITY_OUT");
                break;
            case TelephonyManager.DATA_ACTIVITY_INOUT:
                Log.i(LOG_TAG, "onDataActivity: DATA_ACTIVITY_INOUT");
                break;
            case TelephonyManager.DATA_ACTIVITY_DORMANT:
                Log.i(LOG_TAG, "onDataActivity: DATA_ACTIVITY_DORMANT");
                break;
            default:
                Log.w(LOG_TAG, "onDataActivity: UNKNOWN " + direction);
                break;
        }
    }

    @Override
    public void onServiceStateChanged(ServiceState serviceState) {
        super.onServiceStateChanged(serviceState);
        String message = "onServiceStateChanged: " + serviceState + "\n";
        message += "onServiceStateChanged: getOperatorAlphaLong " + serviceState.getOperatorAlphaLong() + "\n";
        message += "onServiceStateChanged: getOperatorAlphaShort " + serviceState.getOperatorAlphaShort() + "\n";
        message += "onServiceStateChanged: getOperatorNumeric " + serviceState.getOperatorNumeric() + "\n";
        message += "onServiceStateChanged: getIsManualSelection " + serviceState.getIsManualSelection() + "\n";
        message += "onServiceStateChanged: getRoaming " + serviceState.getRoaming() + "\n";
        message += "onServiceStateChanged: " + serviceStateToString(serviceState.getState());
        Log.i(LOG_TAG, message);
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        super.onCallStateChanged(state, incomingNumber);
        callStateToString(state);
        String message = callStateToString(state) + "incomingNumber: " + incomingNumber;
        mTextView.setText(message);
    }

    @Override
    public void onCellLocationChanged(CellLocation location) {
        super.onCellLocationChanged(location);
        String message = "";
        if (location instanceof GsmCellLocation) {
            GsmCellLocation gcLoc = (GsmCellLocation) location;
            message += "onCellLocationChanged: GsmCellLocation " + gcLoc + "\n";
            message += "onCellLocationChanged: GsmCellLocation getCid " + gcLoc.getCid() + "\n";
            message += "onCellLocationChanged: GsmCellLocation getLac " + gcLoc.getLac() + "\n";
            message += "onCellLocationChanged: GsmCellLocation getPsc" + gcLoc.getPsc(); // Requires min API 9
            Log.i(LOG_TAG, message);
        } else if (location instanceof CdmaCellLocation) {
            CdmaCellLocation ccLoc = (CdmaCellLocation) location;
            message += "onCellLocationChanged: CdmaCellLocation " + ccLoc + "\n";
            ;
            message += "onCellLocationChanged: CdmaCellLocation getBaseStationId " + ccLoc.getBaseStationId() + "\n";
            ;
            message += "onCellLocationChanged: CdmaCellLocation getBaseStationLatitude " + ccLoc.getBaseStationLatitude() + "\n";
            ;
            message += "onCellLocationChanged: CdmaCellLocation getBaseStationLongitude" + ccLoc.getBaseStationLongitude() + "\n";
            ;
            message += "onCellLocationChanged: CdmaCellLocation getNetworkId " + ccLoc.getNetworkId() + "\n";
            ;
            message += "onCellLocationChanged: CdmaCellLocation getSystemId " + ccLoc.getSystemId();
            Log.i(LOG_TAG, message);
        } else {
            Log.i(LOG_TAG, "onCellLocationChanged: " + location);
        }
    }

    @Override
    public void onCallForwardingIndicatorChanged(boolean changed) {
        super.onCallForwardingIndicatorChanged(changed);
    }

    @Override
    public void onMessageWaitingIndicatorChanged(boolean changed) {
        super.onMessageWaitingIndicatorChanged(changed);
    }


    public void getCellInfoSignal(Context mContext) {
        if(localMeasurement){



        if (isLocationEnabledCheck(mContext)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                }


                TelephonyManager tm = (TelephonyManager) mContext.getSystemService(TELEPHONY_SERVICE);
                List<CellInfo> cellInfoList = tm.getAllCellInfo();
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                    String signalStrengthlocal = tm.getSignalStrength().toString();
                    parseSignalStrength(signalStrengthlocal);
                }
                measOutput = "";
                if (cellInfoList != null) {
                    for (CellInfo cellInfo : cellInfoList) {
                        boolean cellOutput = false;
                        if ((cellInfo instanceof CellInfoWcdma) && ((mTechnologyFilter && (technologyShowFilter == 2)) || !mTechnologyFilter)) {
                            rsrp = ((CellInfoWcdma) cellInfo).getCellSignalStrength().getDbm();
                            CellIdentityWcdma cellIdentity = ((CellInfoWcdma) cellInfo).getCellIdentity();
                            cellId = (cellIdentity.getCid());
                            technology = "WCDMA";
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                Mcc = cellIdentity.getMccString();
                                Mnc = cellIdentity.getMncString();
                            } else {
                                Mcc = Integer.toString(cellIdentity.getMcc());
                                Mnc = Integer.toString(cellIdentity.getMnc());
                            }

                            Pci = cellIdentity.getPsc();
                            Tac = cellIdentity.getLac();
                            if ((cellInfo).isRegistered()) {
                                isRegistered = true;
                            } else {
                                isRegistered = false;

                            }
                            cellOutput = true;
                        } else if ((cellInfo instanceof CellInfoGsm) && ((mTechnologyFilter && (technologyShowFilter == 0)) || !mTechnologyFilter)) {
                            rsrp = ((CellInfoGsm) cellInfo).getCellSignalStrength().getDbm();
                            CellIdentityGsm cellIdentity = ((CellInfoGsm) cellInfo).getCellIdentity();
                            cellId = cellIdentity.getCid();
                            technology = "GSM";
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                Mcc = cellIdentity.getMccString();
                                Mnc = cellIdentity.getMncString();
                            } else {
                                Mcc = Integer.toString(cellIdentity.getMcc());
                                Mnc = Integer.toString(cellIdentity.getMnc());
                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {

                                Pci = cellIdentity.getBsic();
                            } else {
                                Pci = -1;
                            }
                            Tac = cellIdentity.getLac();
                            if ((cellInfo).isRegistered()) {
                                isRegistered = true;
                            } else {
                                isRegistered = false;

                            }
                            cellOutput = true;

                        } else if ((cellInfo instanceof CellInfoLte) && ((mTechnologyFilter && (technologyShowFilter == 3)) || !mTechnologyFilter)) {
                            CellIdentityLte cellIdentity = ((CellInfoLte) cellInfo).getCellIdentity();
                            rsrp = ((CellInfoLte) cellInfo).getCellSignalStrength().getDbm();
                            cellId = cellIdentity.getCi();
                            technology = "LTE";
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                Mcc = cellIdentity.getMccString();
                                Mnc = cellIdentity.getMncString();
                            } else {
                                Mcc = Integer.toString(cellIdentity.getMcc());
                                Mnc = Integer.toString(cellIdentity.getMnc());
                            }
                            Pci = cellIdentity.getPci();
                            Tac = cellIdentity.getTac();
                            if ((cellInfo).isRegistered()) {
                                isRegistered = true;
                            } else {
                                isRegistered = false;

                            }
                            cellOutput = true;

                        } else if ((cellInfo instanceof CellInfoCdma) && ((mTechnologyFilter && (technologyShowFilter == 1)) || !mTechnologyFilter)) {
                            CellIdentityCdma cellIdentity = ((CellInfoCdma) cellInfo).getCellIdentity();
                            rsrp = ((CellInfoCdma) cellInfo).getCellSignalStrength().getDbm();
                            cellId = cellIdentity.getBasestationId();
                            technology = "CDMA";
                            Mcc = Integer.toString(cellIdentity.getNetworkId());
                            Mnc = Integer.toString(cellIdentity.getSystemId());
                            Pci = -1;
                            Tac = cellIdentity.getBasestationId();

                            if ((cellInfo).isRegistered()) {
                                isRegistered = true;
                            } else {
                                isRegistered = false;

                            }
                            cellOutput = true;

                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            if ((cellInfo instanceof CellInfoNr) && ((mTechnologyFilter && (technologyShowFilter == 4)) || !mTechnologyFilter)) {
                                CellIdentityNr cellIdentity = (CellIdentityNr) ((CellInfoNr) cellInfo).getCellIdentity();
                                rsrp = ((CellInfoLte) cellInfo).getCellSignalStrength().getDbm();
                                cellId = (int) cellIdentity.getNci();
                                technology = "NR";
                                Mcc = cellIdentity.getMccString();
                                Mnc = cellIdentity.getMncString();

                                Pci = cellIdentity.getPci();
                                Tac = cellIdentity.getTac();
                                if ((cellInfo).isRegistered()) {
                                    isRegistered = true;
                                } else {
                                    isRegistered = false;
                                }
                                cellOutput = true;

                            }
                        }
                        if (cellOutput) {
                            if (Mcc==null) {
                                Mcc = "-1";
                                Mnc = "-1";
                            }
                            if (mSwitchOption == 0) {
                                if ((cellId != 2147483647) || (cellId != 65565)) {
                                    processMeasuredData(mSwitchOption);

                                }
                            } else {
                                if ((Pci != 0)) {
                                    processMeasuredData(mSwitchOption);

                                }
                            }

                        }
                    }

                    seriesActivityUpdate();
                }
                checkCellsMeasurementUpdate();
                cellLastDataIndex += 1d;
                Log.i(LOG_TAG, "onCellInfoChanged: " + measOutput);

            }
        }

        }else{
            //sends the message to the server
            if (mTcpClient != null) {
                mTcpClient.sendMessage("testing");
            }
        }

    }

    public static boolean isLocationEnabledCheck(Context context) {

        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            new AlertDialog.Builder(context)
                    .setMessage("Location service not enabled, please enable")
                    .setPositiveButton(R.string.open_location_settings, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton(R.string.Cancel, null)
                    .show();
            return false;
        } else {
            return true;
        }
    }

    public void writeFileOnInternalStorage(Context mcoContext, String sFileName, String sBody, Boolean append) {
        File dir = new File(mStoragePath, mStorageDirectory); ////mContext.getFilesDir()
        if (!dir.exists()) {
            dir.mkdirs();
        }

        try {
            File gpxfile = new File(dir, sFileName);
            FileWriter writer = new FileWriter(gpxfile, append);
            writer.append(sBody);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getMeasurementString() {
        int registered = isRegistered ? 1 : 0;

        return technology + " " + cellId + " " + Mcc + " " + " " + Mnc + " " + " " + Pci + " " + " " + Tac + " " + Integer.toString(rsrp) + " " + rsrpSignalStrength + " " + getDate(timeStamp) + " " + Integer.toString(registered) + "\n";
    }

    private String getDate(long time) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    private void parseSignalStrength(String signalStrengthString) {
        if (!mDeviceModel.equalsIgnoreCase(DEVICE_NAME_SM_T805)) {
            if (signalStrengthString.contains("CellSignalStrengthLte")) {
                final int indexOfRsrp = signalStrengthString.indexOf("rsrp=") + 5;
                final int indexOfRsrpEnd = signalStrengthString.indexOf(" ", indexOfRsrp);
                rsrpSignalStrength = signalStrengthString.substring(indexOfRsrp, indexOfRsrpEnd);

            } else if (signalStrengthString.contains("CellSignalStrengthCdma")) {
                final int indexOfRsrp = signalStrengthString.indexOf("rsrp=");
                final int indexOfRsrpEnd = signalStrengthString.indexOf(" ", indexOfRsrp);
                rsrpSignalStrength = signalStrengthString.substring(indexOfRsrp, indexOfRsrpEnd);
            } else if (signalStrengthString.contains("CellSignalStrengthWcdma")) {
                final int indexOfRsrp = signalStrengthString.indexOf("rscp=") + 5;
                final int indexOfRsrpEnd = signalStrengthString.indexOf(" ", indexOfRsrp);
                rsrpSignalStrength = signalStrengthString.substring(indexOfRsrp, indexOfRsrpEnd);
            } else if (signalStrengthString.contains("CellSignalStrengthGsm")) {
                final int indexOfRsrp = signalStrengthString.indexOf("rsrp=");
                final int indexOfRsrpEnd = signalStrengthString.indexOf(" ", indexOfRsrp);
                rsrpSignalStrength = signalStrengthString.substring(indexOfRsrp, indexOfRsrpEnd);
            } else if (signalStrengthString.contains("CellSignalStrengthNr")) {
                final int indexOfRsrp = signalStrengthString.indexOf("rsrp=");
                final int indexOfRsrpEnd = signalStrengthString.indexOf(" ", indexOfRsrp);
                rsrpSignalStrength = signalStrengthString.substring(indexOfRsrp, indexOfRsrpEnd);
            } else if (signalStrengthString.contains("CellSignalStrengthTdscdma")) {
                final int indexOfRsrp = signalStrengthString.indexOf("rsrp=");
                final int indexOfRsrpEnd = signalStrengthString.indexOf(" ", indexOfRsrp);
                rsrpSignalStrength = signalStrengthString.substring(indexOfRsrp, indexOfRsrpEnd);
            }
        } else {
            String[] rsrpSignalStrengthArr = signalStrengthString.split(" ");
            if (signalStrengthString.contains("lte")) {
                rsrpSignalStrength = rsrpSignalStrengthArr[9];
            } else {
                rsrpSignalStrength = "";
            }

        }


    }

    public void filterMeasurements(int selectedTechnology) {
        technologyShowFilter = selectedTechnology;
        if (mTechnologyFilter) {
            mTechnologyFilter = false;
        } else {
            mTechnologyFilter = true;

        }
    }

    public boolean getTechnologyFilterStatus() {
        return mTechnologyFilter;
    }

    private int getCellIdIndex(List<long[]> cellInfo, int cellId) {
        int index = 0;
        for (long[] list : cellInfo) {
            if (list[0] == cellId) {
                return index;
            } else {
                index++;
            }
        }
        return -1;
    }


    private int getPciIndex(List<long[]> cellInfo, int Pci) {
        int index = 0;
        for (long[] list : cellInfo) {
            if (list[1] == Pci) {
                return index;
            } else {
                index++;
            }
        }
        return -1;
    }


    private long getCellIdSeriesIndex(List<long[]> cellInfo, int series) {
        for (long[] list : cellInfo) {
            if (list[5] == series) {
                return list[0];
            }
        }
        return -1;
    }

    private long getPciSeriesIndex(List<long[]> cellInfo, int series) {
        for (long[] list : cellInfo) {
            if (list[5] == series) {
                return list[1];
            }
        }
        return -1;
    }


    private void writePlotSeries(int rsrp, double graph2LastXValue, int seriesIndex, String seriesLegend) {
        boolean resetSeries = false;
        List<Series> series = mGraph.getSeries();


        if (seriesIndex == -1) {
            //find the unused series and write to it
            int minvalue = Integer.MAX_VALUE;
            int indexMin = -1;
            for (int i = 0; i < 10; i++) {
                if (seriesActivity[i] < minvalue) {
                    minvalue = seriesActivity[i];
                    indexMin = i;
                }
            }
            resetSeries = false;
            seriesIndex = indexMin;
        }

        switch (seriesIndex) {
            case 0: {

                if (resetSeries) {
                    mGraph.removeSeries(mSeries0);
                    mSeries0.resetData(new DataPoint[]{
                            new DataPoint(graph2LastXValue, rsrp)
                    });
                    // legend

                } else {


                    if (!series.contains(mSeries0)) {
                        mGraph.addSeries(mSeries0);
                        mSeries0.setTitle(seriesLegend);
                        //  mSeries0.setColor(colorArray[0]);

                    }
                    mSeries0.appendData(new DataPoint(graph2LastXValue, rsrp), true, 40);

                }
                mSeriesStatus[0] = 1; // series is initialized with data
            }
            break;
            case 1: {

                if (resetSeries) {
                    mGraph.removeSeries(mSeries1);
                    mSeries1.resetData(new DataPoint[]{
                            new DataPoint(graph2LastXValue, rsrp)
                    });
                    // legend

                } else {


                    if (!series.contains(mSeries1)) {
                        mGraph.addSeries(mSeries1);
                        mSeries1.setTitle(seriesLegend);
                        //  mSeries0.setColor(colorArray[0]);

                    }
                    mSeries1.appendData(new DataPoint(graph2LastXValue, rsrp), true, 40);

                }
                mSeriesStatus[1] = 1; // series is initialized with data
            }
            break;
            case 2: {

                if (resetSeries) {
                    mGraph.removeSeries(mSeries2);
                    mSeries2.resetData(new DataPoint[]{
                            new DataPoint(graph2LastXValue, rsrp)
                    });
                    // legend

                } else {


                    if (!series.contains(mSeries2)) {
                        mGraph.addSeries(mSeries2);
                        mSeries2.setTitle(seriesLegend);
                        //  mSeries0.setColor(colorArray[0]);

                    }
                    mSeries2.appendData(new DataPoint(graph2LastXValue, rsrp), true, 40);

                }
                mSeriesStatus[2] = 1; // series is initialized with data
            }
            break;
            case 3: {

                if (resetSeries) {
                    mGraph.removeSeries(mSeries3);
                    mSeries3.resetData(new DataPoint[]{
                            new DataPoint(graph2LastXValue, rsrp)
                    });
                    // legend

                } else {


                    if (!series.contains(mSeries3)) {
                        mGraph.addSeries(mSeries3);
                        mSeries3.setTitle(seriesLegend);
                        //  mSeries0.setColor(colorArray[0]);

                    }
                    mSeries3.appendData(new DataPoint(graph2LastXValue, rsrp), true, 40);

                }
                mSeriesStatus[3] = 1; // series is initialized with data
            }
            break;
            case 4: {

                if (resetSeries) {
                    mGraph.removeSeries(mSeries4);
                    mSeries4.resetData(new DataPoint[]{
                            new DataPoint(graph2LastXValue, rsrp)
                    });
                    // legend

                } else {


                    if (!series.contains(mSeries4)) {
                        mGraph.addSeries(mSeries4);
                        mSeries4.setTitle(seriesLegend);
                        //  mSeries0.setColor(colorArray[0]);

                    }
                    mSeries4.appendData(new DataPoint(graph2LastXValue, rsrp), true, 40);

                }
                mSeriesStatus[4] = 1; // series is initialized with data
            }
            break;
            case 5: {

                if (resetSeries) {
                    mGraph.removeSeries(mSeries5);
                    mSeries5.resetData(new DataPoint[]{
                            new DataPoint(graph2LastXValue, rsrp)
                    });
                    // legend

                } else {


                    if (!series.contains(mSeries5)) {
                        mGraph.addSeries(mSeries5);
                        mSeries5.setTitle(seriesLegend);
                        //  mSeries0.setColor(colorArray[0]);

                    }
                    mSeries5.appendData(new DataPoint(graph2LastXValue, rsrp), true, 40);

                }
                mSeriesStatus[5] = 1; // series is initialized with data
            }
            break;
            case 6: {

                if (resetSeries) {
                    mGraph.removeSeries(mSeries6);
                    mSeries6.resetData(new DataPoint[]{
                            new DataPoint(graph2LastXValue, rsrp)
                    });
                    // legend

                } else {


                    if (!series.contains(mSeries6)) {
                        mGraph.addSeries(mSeries0);
                        mSeries6.setTitle(seriesLegend);
                        //  mSeries0.setColor(colorArray[0]);

                    }
                    mSeries6.appendData(new DataPoint(graph2LastXValue, rsrp), true, 40);

                }
                mSeriesStatus[6] = 1; // series is initialized with data
            }
            break;
            case 7: {

                if (resetSeries) {
                    mGraph.removeSeries(mSeries7);
                    mSeries7.resetData(new DataPoint[]{
                            new DataPoint(graph2LastXValue, rsrp)
                    });
                    // legend

                } else {


                    if (!series.contains(mSeries7)) {
                        mGraph.addSeries(mSeries7);
                        mSeries0.setTitle(seriesLegend);
                        //  mSeries0.setColor(colorArray[0]);

                    }
                    mSeries7.appendData(new DataPoint(graph2LastXValue, rsrp), true, 40);

                }
                mSeriesStatus[7] = 1; // series is initialized with data
            }
            break;
            case 8: {

                if (resetSeries) {
                    mGraph.removeSeries(mSeries8);
                    mSeries8.resetData(new DataPoint[]{
                            new DataPoint(graph2LastXValue, rsrp)
                    });
                    // legend

                } else {


                    if (!series.contains(mSeries8)) {
                        mGraph.addSeries(mSeries8);
                        mSeries0.setTitle(seriesLegend);
                        //  mSeries0.setColor(colorArray[0]);

                    }
                    mSeries8.appendData(new DataPoint(graph2LastXValue, rsrp), true, 40);

                }
                mSeriesStatus[8] = 1; // series is initialized with data
            }
            break;
            case 9: {

                if (resetSeries) {
                    mGraph.removeSeries(mSeries9);
                    mSeries9.resetData(new DataPoint[]{
                            new DataPoint(graph2LastXValue, rsrp)
                    });
                    // legend

                } else {


                    if (!series.contains(mSeries9)) {
                        mGraph.addSeries(mSeries9);
                        mSeries9.setTitle(seriesLegend);
                        //  mSeries0.setColor(colorArray[0]);

                    }
                    mSeries9.appendData(new DataPoint(graph2LastXValue, rsrp), true, 40);

                }
                mSeriesStatus[9] = 1; // series is initialized with data
            }
            break;
            default:
                break;
        }

    }

    private void seriesActivityUpdate() {
        for (int i = 0; i < 10; i++) {
            seriesActivity[i] = seriesActivity[i] - 1;
        }
    }


    private void initSeries() {
        mSeries0.setColor(Color.parseColor(colorArray[0]));
        mSeries1.setColor(Color.parseColor(colorArray[1]));
        mSeries2.setColor(Color.parseColor(colorArray[2]));
        mSeries3.setColor(Color.parseColor(colorArray[3]));
        mSeries4.setColor(Color.parseColor(colorArray[4]));
        mSeries5.setColor(Color.parseColor(colorArray[5]));
        mSeries6.setColor(Color.parseColor(colorArray[6]));
        mSeries7.setColor(Color.parseColor(colorArray[7]));
        mSeries8.setColor(Color.parseColor(colorArray[8]));
        mSeries9.setColor(Color.parseColor(colorArray[9]));
        //mGraph.addSeries(mSeries0);
        mGraph.getLegendRenderer().setVisible(true);
        mGraph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        mGraph.getLegendRenderer().setMargin(200);


    }

    public void setSwitch(int switchOption) {
        mSwitchOption = switchOption;
    }

    public void setSwitchLocalEnb (int switchOption){
        mSwitchLocalEnb = switchOption;
    }

    public boolean ismSwitchEnabled() {
        return mSwitchEnabled;
    }

    public void setSwitchEnabled(boolean switchEnabled) {
        mSwitchEnabled = switchEnabled;
    }


    public void resetGraph() {
        cellLastDataIndex = 1d;

        mSeries0.resetData(new DataPoint[]{
                new DataPoint(cellLastDataIndex, 0)
        });
        mSeries1.resetData(new DataPoint[]{
                new DataPoint(cellLastDataIndex, 0)
        });
        mSeries2.resetData(new DataPoint[]{
                new DataPoint(cellLastDataIndex, 0)
        });
        mSeries3.resetData(new DataPoint[]{
                new DataPoint(cellLastDataIndex, 0)
        });
        mSeries4.resetData(new DataPoint[]{
                new DataPoint(cellLastDataIndex, 0)
        });
        mSeries5.resetData(new DataPoint[]{
                new DataPoint(cellLastDataIndex, 0)
        });
        mSeries6.resetData(new DataPoint[]{
                new DataPoint(cellLastDataIndex, 0)
        });
        mSeries7.resetData(new DataPoint[]{
                new DataPoint(cellLastDataIndex, 0)
        });
        mSeries8.resetData(new DataPoint[]{
                new DataPoint(cellLastDataIndex, 0)
        });
        mSeries9.resetData(new DataPoint[]{
                new DataPoint(cellLastDataIndex, 0)
        });
        mGraph.removeAllSeries();

        for (int ind = 0; ind <10;ind++){
            mSeriesStatus[ind] = 0; //init series status to 0 - no data connected
        }

        //  mGraph.onDataChanged(true,false);
    }


    public void resetGraphSeries(int seriesIndex,int rsrp) {
        cellLastDataIndex = 1d;
        switch (seriesIndex) {
            case 0: {
                mGraph.removeSeries(mSeries0);
                mSeries0.resetData(new DataPoint[]{
                        new DataPoint(cellLastDataIndex, rsrp)
                });
                mGraph.addSeries(mSeries0);
            }

            break;
            case 1: {
                mGraph.removeSeries(mSeries1);
                mSeries1.resetData(new DataPoint[]{
                        new DataPoint(cellLastDataIndex, rsrp)
                });
                mGraph.addSeries(mSeries1);

            }
            break;
            case 2: {
                mGraph.removeSeries(mSeries2);
                mSeries2.resetData(new DataPoint[]{
                        new DataPoint(cellLastDataIndex, rsrp)
                });
                mGraph.addSeries(mSeries2);

            }

            break;
            case 3: {
                mGraph.removeSeries(mSeries3);
                mSeries3.resetData(new DataPoint[]{
                        new DataPoint(cellLastDataIndex, rsrp)
                });
                mGraph.addSeries(mSeries3);

            }
            break;
            case 4: {
                mGraph.removeSeries(mSeries4);
                mSeries4.resetData(new DataPoint[]{
                        new DataPoint(cellLastDataIndex, rsrp)
                });
                mGraph.addSeries(mSeries4);

            }

            break;
            case 5: {
                mGraph.removeSeries(mSeries5);
                mSeries5.resetData(new DataPoint[]{
                        new DataPoint(cellLastDataIndex, rsrp)
                });
                mGraph.addSeries(mSeries5);

            }
            break;
            case 6: {
                mGraph.removeSeries(mSeries6);
                mSeries6.resetData(new DataPoint[]{
                        new DataPoint(cellLastDataIndex, rsrp)
                });
                mGraph.addSeries(mSeries6);

            }

            break;
            case 7: {
                mGraph.removeSeries(mSeries7);
                mSeries7.resetData(new DataPoint[]{
                        new DataPoint(cellLastDataIndex, rsrp)
                });
                mGraph.addSeries(mSeries7);

            }
            break;
            case 8: {
                mGraph.removeSeries(mSeries8);
                mSeries8.resetData(new DataPoint[]{
                        new DataPoint(cellLastDataIndex, rsrp)
                });
                mGraph.addSeries(mSeries8);

            }

            break;
            case 9: {
                mGraph.removeSeries(mSeries9);
                mSeries9.resetData(new DataPoint[]{
                        new DataPoint(cellLastDataIndex, rsrp)
                });
                mGraph.addSeries(mSeries9);

            }
            break;
            default:
                break;
        }

        mGraph.removeAllSeries();

        for (int ind = 0; ind <10;ind++){
            mSeriesStatus[ind] = 0; //init series status to 0 - no data connected
        }

        //  mGraph.onDataChanged(true,false);
    }



    private void processMeasuredData(int parseOption) {
        boolean addCell = false;
        int seriesIndex = -1;
        int infoIndex = -1;
        int mCellInfoIndex = -1;
        boolean plotSeries = true;
        String seriesLegend = "";
        timeStamp = System.currentTimeMillis();

        if (parseOption == 0) {
            seriesLegend = Integer.toString(cellId);

        } else if (parseOption == 1) {
            seriesLegend = Integer.toString(Pci);
        }else if (parseOption == 2) {
            seriesLegend = Integer.toString(Pci);
        }

        if (!mCellInfolist.isEmpty()) {

            // check if cellid/Pci exists in mCellInfoList
            if (parseOption == 0) {
                mCellInfoIndex = getCellIdIndex(mCellInfolist, cellId);

            } else if (parseOption == 1) {
                mCellInfoIndex = getPciIndex(mCellInfolist, Pci);
            }else if (parseOption == 2) {
                mCellInfoIndex = getPciIndex(mCellInfolist, Pci);
            }

            if (mCellInfoIndex != -1) {
                long[] seriesIndexList = mCellInfolist.get(mCellInfoIndex);
                seriesIndex = (int) seriesIndexList[5];
                if(seriesIndex == Integer.MAX_VALUE){
                    seriesIndex = findFreeSeries();
                    if(seriesIndex==Integer.MAX_VALUE){
                        // all is full need to find some series that is either old or has lower rsrp than the one we just got
                        int tmpCellId = -1;
                        if (parseOption == 0) {
                            tmpCellId = (int) checkGraphCellRsrpCellId(rsrp);

                        } else if (parseOption == 1) {
                            tmpCellId = (int) checkGraphCellRsrpPci(rsrp);
                        } else if (parseOption == 2) {
                            tmpCellId = (int) checkGraphCellRsrpPci(rsrp);
                        }
                        if(tmpCellId==-1){
                            plotSeries = false;
                            resetGraphSeries(seriesIndex,rsrp);
                            mCellInfolist.set(tmpCellId,new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),Integer.MAX_VALUE,timeStamp,rsrp});
                            mCellInfolist.add(new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),seriesIndex,timeStamp,rsrp});
                        }else{
                            mCellInfolist.add(new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),Integer.MAX_VALUE,timeStamp,rsrp});
                        }


                    }else if(seriesIndex < 10000){
                        // there is an empty index, so just write there
                        mCellInfolist.set(mCellInfoIndex,new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),seriesIndex,timeStamp,rsrp});

                    }else if(seriesIndex>10000){
                        // clear series index
                        seriesIndex = seriesIndex -10000;
                        if (parseOption == 0) {
                            int tmpCellId = (int) getCellIdSeriesIndex(mCellInfolist,seriesIndex);
                            resetGraphSeries(seriesIndex,rsrp);
                            mCellInfolist.set(tmpCellId,new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),Integer.MAX_VALUE,timeStamp,rsrp});
                        } else if (parseOption == 1) {
                            int tmpPci = (int) getPciSeriesIndex(mCellInfolist,seriesIndex);
                            resetGraphSeries(seriesIndex,rsrp);
                            mCellInfolist.set(tmpPci,new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),Integer.MAX_VALUE,timeStamp,rsrp});

                        }else if (parseOption == 2) {
                            int tmpPci = (int) getPciSeriesIndex(mCellInfolist,seriesIndex);
                            resetGraphSeries(seriesIndex,rsrp);
                            mCellInfolist.set(tmpPci,new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),Integer.MAX_VALUE,timeStamp,rsrp});

                        }
                        mCellInfolist.set(mCellInfoIndex,new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),seriesIndex,timeStamp,rsrp});

                    }
                    mCellInfolist.set(mCellInfoIndex,new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),seriesIndex,timeStamp,rsrp});
                }else{
                    mCellInfolist.set(mCellInfoIndex,new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),seriesIndex,timeStamp,rsrp});

                }
            } else {
                // cellId/PCI exists in mCellInfolist, just update timestamp of last measurement
                seriesIndex = mCellInfolist.size();
                mCellInfolist.add(new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),seriesIndex,timeStamp,rsrp});
            }

        } else {
            seriesIndex = mCellInfolist.size();
            mCellInfolist.add(new long[]{cellId,  Pci,Tac, Integer.parseInt(Mcc), Integer.parseInt(Mnc),seriesIndex,timeStamp,rsrp});
        }

        measOutput = measOutput + "technology: " + technology + ", PCI: " + Pci + ", cellId: " + Integer.toString(cellId) + ", rsrp: " + Integer.toString(rsrp) + ", SSLTE: " + rsrpSignalStrength + "\n";

        writeFileOnInternalStorage(mContext, mFilename, getMeasurementString(), true);
        mTextView.setText(measOutput);
        if(plotSeries){
            writePlotSeries(rsrp, cellLastDataIndex, seriesIndex, seriesLegend);

        }
        //seriesLastValue[seriesIndex] += 1d;
        if(seriesIndex<10){
            seriesActivity[seriesIndex] = SERIES_ACTIVITY_RESET_VALUE;

        }else{
            mTextView.setText("series overflow");
        }
    }

    private void checkCellsMeasurementUpdate(){
        timeStamp = System.currentTimeMillis();
        int index = 0;
        for (long[] list : mCellInfolist) {
            long timeDiff = (timeStamp - list[6]);
            if (timeDiff > CELL_INFO_OUTDATED_THRESHOLD) {
                // flag series for replacement
                //mSeriesFlagged.add(index);
                mSeriesStatus[index] = -1;
                mSeriesStatus[index] = -1; // series can be clearead
            } else {
            }
            index += 1;
        }
    }

    private int findFreeSeries(){
        for(int ind = 0; ind <10; ind++){
            if((mSeriesStatus[ind]==0)){
                return ind;
            }else if(mSeriesStatus[ind]==-1){
                return  100000+ind;
            }

        }
        mSeriesFull = true;
        return Integer.MAX_VALUE;
    }


    private long checkGraphCellRsrpCellId(int rsrp){
        List<long[]> mCellInfolistCopy = new ArrayList<long[]>(mCellInfolist);
        Collections.sort(mCellInfolistCopy,(c1,c2) ->  {
            return (c1[7] > c2[7]) ? 1 : (c1[7] < c2[7] ? -1 : 0);
        });

        for (long[] list : mCellInfolistCopy) {
            if (rsrp > list[7]) {
                return list[0];
            } else {
            }
        }
       return -1;
    }

    private long checkGraphCellRsrpPci(int rsrp){
        List<long[]> mCellInfolistCopy = new ArrayList<long[]>(mCellInfolist);
        Collections.sort(mCellInfolistCopy,(c1,c2) ->  {
            return (c1[7] > c2[7]) ? 1 : (c1[7] < c2[7] ? -1 : 0);
        });

        for (long[] list : mCellInfolistCopy) {
            if (rsrp > list[7]) {
                return list[1];
            } else {
            }
        }
        return -1;
    }


    public class ConnectTask extends AsyncTask<String, String, TcpClient> {

        @Override
        protected TcpClient doInBackground(String... message) {

            //we create a TCPClient object
            mTcpClient = new TcpClient(new TcpClient.OnMessageReceived() {
                @Override
                //here the messageReceived method is implemented
                public void messageReceived(String message) {
                    //this method calls the onProgressUpdate
                    publishProgress(message);
                }
            },SERVER_IP,SERVER_PORT);
           // mTcpClient.configure();
            mTcpClient.run();

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            //response received from server
            String received = Arrays.toString(values);
            if(received.contains("Server MEAS")){
                String meas = "Server MEAS: ";
                String subMeas = received.substring(received.indexOf(meas)+meas.length() +1);
                parseEnbMeas(subMeas);
                mTextView.setText(subMeas);
                Log.d("test", "response " + subMeas);

            }else if(received.contains("Server 600")){
                Log.d("test", "socket closed");

            }else{
                Log.d("test", "response " + received);

            }

            //parse received data
           // processMeasuredData(2);
            //Log.d("test", "response " + values[0]);
            //process server response here....

        }
    }

    public void createTcpConnection(){
        new ConnectTask().execute("");
    }

    public void stopTcpConnection(){
        if (mTcpClient != null) {
            mTcpClient.sendStringByte("BYE");
            mTcpClient.stopClient();
        }
    }

    public void startEnbMeasurement( String serverIp,int serverPort ){
        SERVER_IP = serverIp;
        SERVER_PORT = serverPort;
        createTcpConnection();
        while(mTcpClient == null){

        }
        while(!mTcpClient.ismSocketInitialized()){
            // socket initialized we can continue
            Toast.makeText(mContext, "socket initialized", Toast.LENGTH_SHORT).show();

        }
    }


    public void collectFromSocket(){

        mTcpClient.sendStringByte("COLLECT");
    }

    private void parseEnbMeas (String meas){
        String lookupStringCellid = "Cell Id: ";
        String lookupStringRsrp = "RSRP: ";
        String[] split = meas.split(";");
        boolean measData = false;
        for(String m : split){
            if(m.contains(lookupStringCellid)){
                String inCellId = m.substring(m.indexOf(lookupStringCellid)+lookupStringCellid.length(),m.indexOf(",",1));
                String inRsrp = m.substring(m.indexOf(lookupStringRsrp)+lookupStringRsrp.length());

                rsrp = Integer.parseInt(inRsrp);
                cellId = Integer.parseInt(inCellId);
                technology = "LTE";
                Pci = cellId;
                Tac = -1;

                switch (cellId) {
                    case 0:
                        Mcc = "208";
                        Mnc = "92";
                        isRegistered = true;
                        measData = true;
                        break;
                    case 31:
                        Mcc = "230";
                        Mnc = "31";
                        isRegistered = false;
                        measData = true;
                        break;
                    case 32:
                        Mcc = "230";
                        Mnc = "32";
                        isRegistered = false;
                        measData = true;
                        break;
                    case 33:
                        Mcc = "230";
                        Mnc = "33";
                        isRegistered = false;
                        measData = true;
                        break;
                    default:
                        Mcc = "-1";
                        Mnc = "-1";
                        isRegistered = false;
                        measData = true;
                        break;
                }


            if(measData){
                processMeasuredData(2);

            }

            }

        }
        seriesActivityUpdate();
        cellLastDataIndex += 1d;
        checkCellsMeasurementUpdate();
    }
}
