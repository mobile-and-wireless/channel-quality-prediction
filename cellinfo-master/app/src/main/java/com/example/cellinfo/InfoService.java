package com.example.cellinfo;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import static com.example.cellinfo.App.CHANNEL_ID;

/**
 * Author: Jan Plachy (plachyjan@gmail.com)
 */
public class InfoService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
       // startForeground(ID_SERVICE, notification);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
       // String input = intent.getStringExtra("inputExtra");
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        notificationIntent.setAction(Intent.ACTION_MAIN);
       // notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("CellInfo ongoing measurement")
                //.setContentText(input)
                .setSmallIcon(R.drawable.ic_cellinfo)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        //do heavy work on a background thread
        //stopSelf();
        return START_NOT_STICKY;
    }


    public void onDestroy(){
        super.onDestroy();

        // measurement.stopMeasuring();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
