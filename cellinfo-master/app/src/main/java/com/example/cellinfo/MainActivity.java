package com.example.cellinfo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.List;



// TODO:
//    Measurements of neighboring cells needs check
//    NetworkScanRequest
//    Visualization of measured data
//    Upload measured data to DB
//    Communication with ML -> push data to DB, enforce ML learning, send current measurement and get predicted channel quality
//    Exploit fragments to handle device orientation change and similar events
//    Cleanup of code
//    Support for storing on Lineage OS and similar

public class MainActivity extends AppCompatActivity {

    //--------------------------------------------------
    // Constants
    //--------------------------------------------------

    private static final String[] PERMISSIONS = { Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_COARSE_LOCATION };
    private static final int PERMISSION_REQUEST = 100;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static final String LOG_TAG = "PhoneCallback";
    public static final String BUTTON_MEAS_START = "Start measuring";
    public static final String BUTTON_MEAS_STOP = "Stop measuring";
    private static final String[] TECHNOLOGY_STRING_ARRAY = {"GSM","CDMA","WCDMA","LTE","NR"};

    //--------------------------------------------------
    // Attributes
    //--------------------------------------------------

    private TelephonyManager mTelephonyManager;
    private Context mContext;
    private TextView mTextView;
    private PhoneStateListener customPhoneStateListener;
    private PhoneCallback phoneCallback;

    //--------------------------------------------------
    // Activity Life Cycle
    //--------------------------------------------------
    private int measuring = 0;

    private String  mFilenameSave = "";
    private Button mButtonControlMeasurement;
    private EditText mFileNameSave;
    private EditText mServerIp;
    private EditText mServerPort;
    private TextView mainTextView;
    private String mDeviceModel;
    private Spinner mSpinner;
    private Button mButtonFilter;
    private GraphView mGraph;
    private int mSwitchOption = 0;
    private Switch mSwitch;
    private Switch mSwitchLocalEnb;
    private int ongoingMeasurementIdType = -1;
    private int local_enb = 0;
    private String SERVER_IP = "";
    private int SERVER_PORT = 1324;
    private boolean mDebug = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                1);

        mTextView = findViewById(R.id.textView);
        mButtonControlMeasurement = findViewById(R.id.button_control_measurement);
        mFileNameSave = findViewById(R.id.editTextFilenameSave);
        mServerIp = findViewById(R.id.editTextServerIP);
        mServerPort = findViewById(R.id.editTextPort);
/*
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (end > start) {
                    String destTxt = dest.toString();
                    String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
                    if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                        return "";
                    } else {
                        String[] splits = resultingTxt.split("\\.");
                        for (int i=0; i<splits.length; i++) {
                            if (Integer.valueOf(splits[i]) > 255) {
                                return "";
                            }
                        }
                    }
                }
                return null;
            }
        };
        text.setFilters(filters);
        */

        mDeviceModel = getDeviceName();
        mGraph = findViewById(R.id.graph);
        mSwitch = findViewById(R.id.switch_cellId_pci);
        mSwitchLocalEnb = findViewById(R.id.switch_local_remote);

        mGraph.getViewport().setXAxisBoundsManual(true);
        mGraph.getViewport().setMinX(0);
        mGraph.getViewport().setMaxX(40);
        mGraph.getViewport().setMinY(-140);
        mGraph.getViewport().setMaxY(-50);
       // mSeries.appendData(new DataPoint(0, -140), true, 40);

        mSpinner = findViewById(R.id.spinner_technology_filter);
        List<String> spinnerArray =  new ArrayList<String>();

        for(String spinnerTech : TECHNOLOGY_STRING_ARRAY){
            spinnerArray.add(spinnerTech);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mButtonFilter = findViewById(R.id.button_filter_measurement);
        phoneCallback = new PhoneCallback(mTextView,mContext,mFilenameSave,mDeviceModel,mSpinner,mGraph);
        mTelephonyManager = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);


    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putInt("measuring",measuring);
        savedInstanceState.putString("mFilenameSave",mFilenameSave);


        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        measuring = savedInstanceState.getInt("measuring");

        if(measuring==1){
            mFileNameSave.setEnabled(true);
        }else{
            mFileNameSave.setEnabled(false);
            mFilenameSave.replaceAll("",mFilenameSave);
            boolean locationAccess = checkLocationPermission();

            if(locationAccess){
                Intent intent = new Intent(this, InfoService.class);
                ContextCompat.startForegroundService(this,intent);

                callPhoneManager();
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                mFileNameSave.setEnabled(false);
                mButtonControlMeasurement.setText(BUTTON_MEAS_STOP);
                measuring = 1;
            }
        }
    }
    //--------------------------------------------------
    // Permissions
    //--------------------------------------------------

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Requesting location")
                        .setMessage("Please provide location permission for measurements")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        LocationManager locationManager = null;
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 400, 1, (LocationListener) this);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(MainActivity.this, "Permission granted", Toast.LENGTH_SHORT).show();

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
            default:
                throw new IllegalStateException("Unexpected value: " + requestCode);
        }
    }

    public static boolean isLocationEnabled(Context context) {

        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            new AlertDialog.Builder(context)
                    .setMessage("Location service not enabled, please enable")
                    .setPositiveButton(R.string.open_location_settings, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton(R.string.Cancel, null)
                    .show();
            return false;
        }else{
            return true;
        }
    }

    private void callPhoneManager() {
        //TextView textView = (TextView)findViewById(R.id.textView);
        //mTelephonyManager = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);


        mainTextView = (TextView) findViewById(R.id.textView);

        customPhoneStateListener = phoneCallback; //new PhoneCallback(mainTextView,mContext,mFilenameSave,mDeviceModel);
        mTelephonyManager.listen(customPhoneStateListener,
                PhoneStateListener.LISTEN_CELL_INFO
                        |PhoneStateListener.LISTEN_SIGNAL_STRENGTHS
                        | PhoneStateListener.LISTEN_CALL_STATE
                        | PhoneStateListener.LISTEN_CELL_LOCATION
                        | PhoneStateListener.LISTEN_DATA_ACTIVITY
                        | PhoneStateListener.LISTEN_DATA_CONNECTION_STATE
                        | PhoneStateListener.LISTEN_SERVICE_STATE
                        | PhoneStateListener.LISTEN_CALL_FORWARDING_INDICATOR
                        | PhoneStateListener.LISTEN_MESSAGE_WAITING_INDICATOR
                       );


    }

    public void manualMeasurement(View view){

        phoneCallback.getCellInfoSignal(mContext);

    }



    public void buttonControlMeasuring (View view){
        String tmp = (String) mSwitch.getText();
        boolean clickHandled = false;
        switch(measuring){
            case 0: {
                if(!clickHandled){
                    mFilenameSave = String.valueOf(mFileNameSave.getText());
                    boolean locationAccess = checkLocationPermission();

                    if(locationAccess){
                        //check the switch
                        if(mSwitch.isChecked()){
                            mSwitchOption = 1;
                        }else{
                            mSwitchOption = 0;

                        }

                        if(local_enb == 1){
                            if((mTelephonyManager != null)&&(customPhoneStateListener!= null)){
                                mTelephonyManager.listen(customPhoneStateListener, PhoneStateListener.LISTEN_NONE);
                            }
                        }else{


                        }
                        phoneCallback.setSwitch(mSwitchOption);
                        if(ongoingMeasurementIdType!=-1){

                            if(ongoingMeasurementIdType!=mSwitchOption){
                                //reset graph and start new measurements
                                phoneCallback.resetGraph();
                                //mTelephonyManager.listen(customPhoneStateListener,PhoneStateListener.LISTEN_NONE);
                                //customPhoneStateListener = null;
                            }
                        }
                        Intent intent = new Intent(this, InfoService.class);
                        ContextCompat.startForegroundService(this,intent);
                        ongoingMeasurementIdType = mSwitchOption;
                        if(local_enb == 0) {
                            callPhoneManager();
                        }else{
                            phoneCallback.startEnbMeasurement(SERVER_IP,SERVER_PORT);
                        }
                        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        mFileNameSave.setEnabled(false);
                        mButtonControlMeasurement.setText(BUTTON_MEAS_STOP);
                        measuring = 1;
                        mSwitch.setClickable(false);
                        phoneCallback.setSwitchEnabled(false);

                    }
                    clickHandled = true;
                }

            }
            case 1: {
                if(!clickHandled) {
                    mFileNameSave.setEnabled(true);
                    mButtonControlMeasurement.setText(BUTTON_MEAS_START);
                    Intent intent = new Intent(this, InfoService.class);
                    stopService(intent);
                    if(local_enb == 0) {
                        mTelephonyManager.listen(customPhoneStateListener,PhoneStateListener.LISTEN_NONE);
                    }else{
                        phoneCallback.stopTcpConnection();
                    }
                    customPhoneStateListener = null;
                    measuring = 0;
                    clickHandled = true;
                    mSwitch.setClickable(true);
                    phoneCallback.setSwitchEnabled(true);

                }
            }
        }

    }

    public void exitApp(View view){
        if(measuring == 1){

            Intent intent = new Intent(this, InfoService.class);
            stopService(intent);
            if(local_enb == 0) {
                mTelephonyManager.listen(customPhoneStateListener,PhoneStateListener.LISTEN_NONE);

            }else{
                phoneCallback.stopTcpConnection();

            }
            customPhoneStateListener = null;

        }
        finishAndRemoveTask();
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    public void buttonSetTechnologyFilter(View view){
        //throw new RuntimeException("Test Crash"); // Force a crash for crashanalytics

        if(phoneCallback.getTechnologyFilterStatus()){
            mButtonFilter.setText("set filter");

        }else{
            mButtonFilter.setText("Disable filter");

        }
        int selectedTechnology = mSpinner.getSelectedItemPosition();
        phoneCallback.filterMeasurements(selectedTechnology);

    }


    public void switchOption(View view){

        if(!phoneCallback.ismSwitchEnabled()) {
            Toast.makeText(MainActivity.this, "Cannot change switch during measurement", Toast.LENGTH_LONG).show();
        }


        if(mSwitchOption == 1){
            mSwitchOption = 0;

        }else{
            if(phoneCallback.ismSwitchEnabled()){
                mSwitchOption = 1;

            }else{
               // Toast.makeText(MainActivity.this, "Cannot change switch now", Toast.LENGTH_SHORT).show();

            }
        }
        phoneCallback.setSwitch(mSwitchOption);

    }

    public void switchLocalEnb(View view){
        if(mDebug){
            SERVER_IP = "192.168.27.124"; //192.168.27.111
            SERVER_PORT = 1234;
        }else{
            SERVER_IP= mServerIp.getText().toString();
            SERVER_PORT = Integer.parseInt(mServerPort.getText().toString());
        }




        if(local_enb == 1){
            local_enb = 0;
            mSwitch.setClickable(true);

        }else{
            if(phoneCallback.ismSwitchEnabled()){
                local_enb = 1;
                mSwitch.setClickable(false);
                mSwitchOption = 1;
                mSwitch.setChecked(true);

            }else{
                // Toast.makeText(MainActivity.this, "Cannot change switch now", Toast.LENGTH_SHORT).show();

            }
        }
        phoneCallback.setSwitchLocalEnb(local_enb);
    }


    public void buttonCollectFromSocket(View view){
        phoneCallback.collectFromSocket();
    }
}