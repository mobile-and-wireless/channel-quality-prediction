package com.example.cellinfo;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.telephony.AccessNetworkConstants;
import android.telephony.CellInfo;
import android.telephony.NetworkScanRequest;
import android.telephony.RadioAccessSpecifier;
import android.telephony.TelephonyManager;
import android.telephony.TelephonyScanManager;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Author: Jan Plachy (plachyjan@gmail.com)
 */
@RequiresApi(api = Build.VERSION_CODES.P)
public class RadioCallback extends TelephonyScanManager.NetworkScanCallback {
    private List<CellInfo> mCellInfoResults;
    private int mScanError;
    private TextView mTextView;
    private Context mContext;

    public RadioCallback(TextView textView, Context context) {
        mTextView = textView;
        mContext = context;
    }

    @Override
    public void onResults(List<CellInfo> cellInfoResults) {
        mCellInfoResults = cellInfoResults;

                for (CellInfo cellInfo:mCellInfoResults) {
                    mTextView.setText(cellInfo.toString() + "\n");

                }


    }

    @Override
    public void onError(int error) {
        mScanError = error;

    }

    @Override
    public void onComplete() {

    }



    @RequiresApi(api = Build.VERSION_CODES.P)
    public void runNetworkScanner() {

        NetworkScanRequest networkScanRequest;
        RadioAccessSpecifier radioAccessSpecifiers[];
        int bands[]  =new int[1];

        ArrayList<String> PLMNIds = new ArrayList<String>();
        PLMNIds.add("20892");
        PLMNIds.add("23031");
        PLMNIds.add("23032");
        PLMNIds.add("23033");


        TelephonyManager telephonyManager = (TelephonyManager) mContext.getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        bands[0]= AccessNetworkConstants.EutranBand.BAND_7;
        radioAccessSpecifiers = new RadioAccessSpecifier[1];
        radioAccessSpecifiers[0] = new RadioAccessSpecifier(
                AccessNetworkConstants.AccessNetworkType.EUTRAN,
                bands,
                null);

        networkScanRequest = new NetworkScanRequest(
                NetworkScanRequest.SCAN_TYPE_ONE_SHOT,
                radioAccessSpecifiers,
                10,
                10,
                false,
                1,
                PLMNIds);

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        telephonyManager.requestNetworkScan(networkScanRequest, AsyncTask.SERIAL_EXECUTOR, new RadioCallback(mTextView,mContext));
    }
}