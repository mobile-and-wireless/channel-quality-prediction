#relu: for 100 epochs loss is ~1.79788; for 200 epochs loss is ~1.19405; for 300 epochs loss is ~1.35318; for 400 epochs loss is 0.89578; for 500 epochs loss is 0.97303

def parse_input_rats(headers,dnn_mdl):
    #according to headers, insert wifi and mobile network cells to the DNN model
    dnn_mdl.number_inputs_cell = 0
    dnn_mdl.number_inputs_wifi = 0
    for h in headers:
        if "cell" in h:
            dnn_mdl.number_inputs_cell += 1
        if "wifi" in h:
            dnn_mdl.number_inputs_wifi += 1 

    dnn_mdl.number_inputs_total = dnn_mdl.number_inputs_cell + dnn_mdl.number_inputs_wifi   


def drop_dataset_columns():
    #drop all the collumns from dataset
    drop_string_columns =  ''

    for column in dnn_mdl.columns_drop_mobile:
        mobileString = 'cell'
        drop_string = mobileString + str(column) 
        dnn_mdl.dataset = dnn_mdl.dataset.drop(drop_string,axis =1 ,errors ='ignore')
        drop_string_columns = drop_string_columns + str(column)
        if(column != dnn_mdl.columns_drop_mobile[len(dnn_mdl.columns_drop_mobile) -1]):
            drop_string_columns = drop_string_columns + ','


    for column in dnn_mdl.columns_drop_wifi:
        wifiString = 'wifi'
        drop_string = wifiString + str(column) 
        dnn_mdl.dataset = dnn_mdl.dataset.drop(drop_string,axis =1 ,errors ='ignore')
        drop_string_columns = drop_string_columns + str(column)
        if(column != dnn_mdl.columns_drop_wifi[len(dnn_mdl.columns_drop_wifi) -1]):
            drop_string_columns = drop_string_columns + ','
    
    dnn_mdl.drop_string = drop_string_columns

def setup_data():
    dnn_mdl.train_dataset = dnn_mdl.dataset.sample(frac=0.8, random_state=0)
    dnn_mdl.test_dataset = dnn_mdl.dataset.drop(dnn_mdl.train_dataset.index)

    dnn_mdl.train_features = dnn_mdl.train_dataset.copy()
    dnn_mdl.test_features = dnn_mdl.test_dataset.copy()

    #removing columns "cell0"
    dnn_mdl.train_labels = dnn_mdl.train_features.pop('cell0')
    dnn_mdl.test_labels = dnn_mdl.test_features.pop('cell0')


def normalize_input_data(data):
    #normalize input data almost <-1,1> 
    return (data - 100)/10


import sys
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd # working with sheets
import seaborn as sns # advanced vizualization
from pathlib import Path # working with directories
import utils.Model as model

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras # API for working with models
from keras.regularizers import l1 # regulation for overlearning (big arguments)
from keras.regularizers import l2
from utils.dnnToJSON import export_dnn, export_dnn_results

# Make numpy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)

#set deterministic behavior for numpy and tensorflow
fixed_seed = True
if fixed_seed:
    np.random.seed(1)
    tf.random.set_seed(2)

plotComparison = True
plotHistory = False
normalize = False
rsrp_threshold = -110
train_percentage = 0.7

#creating new model, set the columns which will be dropped?
dnn_mdl = model.Model()
dnn_mdl.columns_drop_wifi =[] #[*range(0,44)]
dnn_mdl.columns_drop_mobile = [] # [*range(6,39)]
dnn_mdl.number_inputs_train = 53

#path to folders
data_folder_npy = Path("data/npy/")
data_folder_txt = Path("data/txt/")
data_folder_csv = Path("data/csv/")
data_folder_mdl = Path("data/mdl/")

#path to files
# file_data_csv = data_folder_csv / "dataFilter.csv"
# file_data= data_folder_txt / "dataOMF.txt"
# filename = "data1int.txt" 
filename = "dmod1.txt" 
filename_out_append = 'tl5zero22'
# file_dataFilter= data_folder_txt / "dataOFilterAugment.txt"
# file_dataFilterAugment= data_folder_txt / "dataOMFilterAugment2.txt"
file_data_header = data_folder_txt / "dataMeas2FilterAugment_header.txt"
file_data_combo = data_folder_txt /"comboSelectedRemovalOMTransfer.txt"
file_data_comboCompare = data_folder_txt /"comboSelectedRemovalCompareOMTransfer.txt"
# file_test_mdl = data_folder_mdl /"modelOM.cp.ckpt"
# file_test_mdlJson = data_folder_mdl /"modelOM.json"
file_save_model = data_folder_mdl /"modelBasic.tf"
file_save_model2 = data_folder_mdl /"modelTransfer.tf"

if len(sys.argv) > 1:
    file_datamod=data_folder_txt / sys.argv[1] # if specified by input
else:
    file_datamod= data_folder_txt / "dataInt.txt" # default

#read list of cells and tranfer it to array (headers for next CSV file)
f = open(file_data_header)
data_Np_Header = f.read()
headers = data_Np_Header.split(';')
headers.pop() 

raw_dataset = pd.read_csv(file_datamod, names=headers,
                        na_values='?', comment='\t',
                        sep=',', skipinitialspace=False)


if(normalize):
    dnn_mdl.dataset = normalize_input_data(abs(raw_dataset.copy()))
else:
    dnn_mdl.dataset = abs(raw_dataset.copy())

dnn_mdl.number_inputs_wifi = dnn_mdl.dataset.shape[1]
parse_input_rats(headers,dnn_mdl)
drop_dataset_columns()
setup_data()


load_mdl  = tf.keras.models.load_model(file_save_model)
load_mdl.pop() #remove last layer
load_mdl.pop() #remove last layer
load_mdl._name = 'base'
for i in range(4):
    load_mdl.layers[i].trainable = False

for i in range(4,5):
    load_mdl.layers[i].trainable = True

ll = load_mdl.layers[5].output #it takes output from 5th layer
ll = layers.Dense(20,name='topOut2')(ll)
ll = layers.Dense(4,name='topOut1')(ll)
ll = layers.Dense(1,name='topOut0')(ll)
ll._name = 'top' #name of output
dnn_mdl.dnn_cell_model = tf.keras.Model(inputs=load_mdl.input,outputs=ll) #creating new model (joining 2 models)


dnn_mdl.dnn_cell_model.compile(loss='mean_squared_error',
                optimizer=tf.keras.optimizers.Nadam(0.001),metrics=['mean_absolute_error'])
#last layer training only
history = dnn_mdl.dnn_cell_model.fit(
    dnn_mdl.train_features, dnn_mdl.train_labels,
    validation_split=0.2,
    validation_data=(dnn_mdl.test_features, dnn_mdl.test_labels),
    verbose=1, epochs=3), #20

# unfreeze reused layers
for i in range(4):
    load_mdl.layers[i].trainable = True 

dnn_mdl.dnn_cell_model.compile(loss='mean_squared_error',
                optimizer=tf.keras.optimizers.Nadam(0.0001),metrics=['mean_absolute_error'])
# second training of all layers with lower "step"
history = dnn_mdl.dnn_cell_model.fit(
    dnn_mdl.train_features, dnn_mdl.train_labels,
    validation_split=0.2,
    validation_data=(dnn_mdl.test_features, dnn_mdl.test_labels),
    verbose=1, epochs=5), #180

dnn_mdl.dnn_cell_model.save(file_save_model2)
# Plot history (also known as a loss curve)
pd.DataFrame(history[0].history).plot()
plt.ylabel("loss")
plt.xlabel("epochs");

#mean absolute error
dnn_mdl.predicted_features = dnn_mdl.dnn_cell_model.predict(dnn_mdl.test_features)
print("predicted")
print(dnn_mdl.predicted_features)
loss_mae = np.sum(abs(np.transpose((dnn_mdl.predicted_features)) - np.array(dnn_mdl.test_labels)))/len(dnn_mdl.predicted_features)
loss_mae_round = np.sum(np.round(abs(np.transpose((dnn_mdl.predicted_features)) - np.array(dnn_mdl.test_labels))))/len(dnn_mdl.predicted_features)

#pearson correlation
test_labels_list = np.array(dnn_mdl.test_labels)
pred_labels_list = np.squeeze(np.array(dnn_mdl.predicted_features.tolist()))
my_rho = np.corrcoef(test_labels_list, pred_labels_list)

# export_dnn_results(loss_mae, loss_mae_round, my_rho[1][0])

print('removed %s Loss %f (rounded loss %f) Pearson: %f' % (dnn_mdl.drop_string,loss_mae,loss_mae_round,my_rho[1][0]))
out = 'trained: ' + dnn_mdl.drop_string + ' loss: ' + str(loss_mae) + '\n'
compare = ''

for idx in range(len(dnn_mdl.predicted_features)):
    compare = compare + str(dnn_mdl.predicted_features[idx]) + ',' + str(test_labels_list[idx]) + '\n'

file_data_comboCompare

#overrite the input file with the resulting data
fin = open(file_data_comboCompare, "wt")
fin.write(compare)
fin.close()

#overrite the input file with the resulting data
fin = open(file_data_combo, "a")
fin.write(out)
fin.close()

if plotComparison:
    data_folder_png = Path("data/plot/png/")
    data_png = filename[0:len(filename)-4] + "_testPredictCompareAllCells_" +filename_out_append + ".png"
    file_data_png = data_folder_png / data_png
    data_err = filename[0:len(filename)-4] + "_testPredictCompareAllCellsErr_" +filename_out_append + "..png"
    file_data_png_err = data_folder_png / data_err

    data_png_rounded = filename[0:len(filename)-4] + "_testPredictCompareAllCellsRounded_" +filename_out_append + "..png"
    file_data_png_rounded = data_folder_png / data_png_rounded

    data_diff = filename[0:len(filename)-4] + "_testPredictCompareDiffAllCells_" +filename_out_append + "..png"
    file_data_diff_png = data_folder_png / data_diff
    data_diff_rounded = filename[0:len(filename)-4] + "_testPredictCompareDiffAllCellsRounded_" +filename_out_append + "..png"
    file_data_diff_png_rounded = data_folder_png / data_diff_rounded

    stringTitle = "Pearson: %.2f MAE: %.2f dB (MAE rounded: %.2f dB" % (my_rho[1][0], loss_mae,loss_mae_round)
    plt.figure(1)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("RSRP [dBm]") 
    plt.plot(test_labels_list, label='test')
    plt.plot(pred_labels_list, label='predicted')
    plt.legend() 
    plt.savefig( file_data_png, dpi=300)
    plt.show()


    plt.figure(2)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("RSRP [dBm]") 
    plt.plot(test_labels_list-pred_labels_list, label='test')
    plt.legend() 
    plt.savefig( file_data_diff_png, dpi=300)
    plt.show()


    plt.figure(3)
    sns.histplot((test_labels_list-pred_labels_list), kde=True, stat= 'probability',
             bins=30, color = 'darkblue')
    #sns.displot((test_labels_list-pred_labels_list), kde=True, 
    #         bins=30, color = 'darkblue')
    plt.xlabel("error [dB]") 
    plt.ylabel("Density [-]") 
    plt.legend() 
    plt.savefig( file_data_png_err, dpi=300)
    plt.show()


if plotHistory:
    plt.figure(4) 
    plt.plot(history[0].history['accuracy'])
    plt.plot(history[0].history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
    # summarize history for loss
    plt.figure(5) 
    plt.yscale("log") 
    plt.plot(history[0].history['loss'])
    plt.plot(history[0].history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
