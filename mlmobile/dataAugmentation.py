

def  filterRsrp(file_data,file_dataFilter,rsrp_threshold):
    fin = open(file_data, "rt")
    data = fin.read()

    file_data= data_folder_txt / "dataOMFilter2.txt"
    fout = open(file_dataFilter, "wt")

    lines = data.split('\n')
    for line in lines:
        if line:
            if not "cell" in line:
                rsrp_val = line.split(',')[0]
                if not int(rsrp_val)<rsrp_threshold:
                    fout.write(line+'\n')

    fout.close()
    fin.close()


def  augmentData(file_dataFilter,file_dataFilterAugment,record_augmentation):
    fin = open(file_dataFilter, "rt")
    data = fin.read()

    fout = open(file_dataFilterAugment, "wt")

    lines = data.split('\n')
    for line in lines:
        fout.write(line+ '\n')
        if line:
            if not "cell" in line:
                rsrp_val = line.split(',')
                for augmentation in range(record_augmentation):
                    cellId = 1
                    for val in rsrp_val:
                        if cellId>1:
                            if int(val) != 0:
                                value = randint(-spread,spread)
                            else:
                                value =0    
                            fout.write(','+ str(int(val)+value))
                        else:
                            fout.write(val)

                        cellId+=1
                    fout.write('\n')
                
    fout.close()
    fin.close()


def remove_stuck(file_data,file_data_nostuck,remove_stuck_index):
    fin = open(file_data, "rt")
    data = fin.read()

    fout = open(file_data_nostuck, "wt")
    lineIdx = 0
    lines = data.split('\n')
    for line in lines:
        if (lineIdx<remove_stuck_index[0]) or (lineIdx>remove_stuck_index[1]) :
            fout.write(line+'\n')
        lineIdx +=1
    fout.close()
    fin.close()
	 


from random import seed
from random import randint
from pathlib import Path
import sys

remove_stuck_data = 1
remove_stuck_index = [5215,7000]
spread = 1
seed(1)
record_augmentation = 1
rsrp_threshold = -120
data_folder_txt = Path("data/txt/")
#print(sys.argv[0])
file_data= data_folder_txt / sys.argv[1] #"data5shadowint.txt" #"dataO2.txt"
#print(file_data)
file_data_nostuck= data_folder_txt / "dataONS.txt"
file_dataFilter= data_folder_txt / "dataOFilter.txt"
file = sys.argv[1].replace('.txt', '')
file_dataFilterAugment= data_folder_txt / (file+"_aug.txt") #"data5FilterAugment.txt"

if remove_stuck_data:
    remove_stuck(file_data,file_data_nostuck,remove_stuck_index)

filterRsrp(file_data_nostuck,file_dataFilter,rsrp_threshold)
augmentData(file_dataFilter,file_dataFilterAugment,record_augmentation)