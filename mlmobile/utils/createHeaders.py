from pathlib import Path

nCell = 5
nWifi = 45
cellStr = 'cell'
wifiStr = 'wifi'

data_folder_txt = Path("data/txt/")


file_output= data_folder_txt / "dataNpHeader.txt"



outString = ''
separatingChar = ';'

for i in range(nCell):
    outString += cellStr + str(i) + separatingChar

for w in range(nWifi):
    outString += wifiStr + str(w) + separatingChar

 #open the input file in write mode
fin = open(file_output, "wt")
#overrite the input file with the resulting data
fin.write(outString)
#close the file
fin.close()   

