from pathlib import Path
import re
from itertools import count
import utils.MeasurementStrings as ms
import json

def replace_strings(data):
    counter = count(mIdx)   
    #data = data.replace('{"measurements":\n\n}','')
    #data = data.replace('}]}', '}\t\n        ]\t\n    }\t\n}')
    data = data.replace('},{"technology"','},\n\t\t\t{"technology"')
    data = data.replace('}]},{"m','}],\n\t\t"m')
    #data = data.replace('},{,{','},\n\t{')
    data = data + '\t\n    }\t\n}'
    #data = re.sub(r'@', lambda x: str(next(counter)), data )

    return data

def proces_string(string,m_string):
    m_start = string.index('"m')
    string = string[m_start:]

    if m_string.string_empty_meas in string:
        m_empty = string.index(m_string.string_empty_meas)
        string_sandbox = string[m_empty-search_char_n_back:]
        m_start_empty = string_sandbox.index(m_string.string_empty_meas_start)
        string = string[:m_empty-search_char_n_back+m_start_empty]

    string_end = string[len(string)-search_char_n_back:]
    m_end = string_end.index(']')
    string = string[:len(string)-search_char_n_back+m_end+1]
  
    return string

def remove_empty_measurements(string):
    data_json = json.loads(string)
    meas_data = data_json['measurements']
    remove = []
    for m in meas_data:
        if len(meas_data[m])<2:
            remove.append(m)

    for r in remove:
        del data_json['measurements'][r]  

    return json.dumps(data_json, indent=4)


    

nofix = True
mIdx = 0
measCount = 0
data_folder_rooms = Path("data/rooms/")
data_folder_json = Path("data/json/")
data_folder_rooms_json = Path("data/roomjson/")
filesToProcess= {"p1meas","p2b.measmeas","p3meas","p4correctmeas","p5correctmeas","p6meas","p7meas","p8meas"}

for file in filesToProcess:
    filenameIn = file + ".json"

    file_measureAll = data_folder_rooms / filenameIn
    m_string = ms.MeasurementStrings()

    #read input file
    fin = open(file_measureAll, "rt")
    #read file contents to string
    data = fin.read()

    data = replace_strings(data)
        
    # count number of measures and split it to separate files
    measureS_split = data.split('{"measurements":')

    #close the input file
    fin.close()
    search_char_n_back = 50
    for mI in range(len(measureS_split)):
        if('"m' in measureS_split[mI]):
            string =proces_string(measureS_split[mI],m_string)

            string_file =  "Out"+ file + str(mI) +'.json'
            string_out = m_string.string_file_start + string + m_string.string_file_end
            string_out = remove_empty_measurements(string_out)
            file_mWjson = data_folder_rooms_json / string_file
            #open the input file in write mode
            fin = open(file_mWjson, "wt")
            #overrite the input file with the resulting data
            fin.write(string_out)
            #close the file
            fin.close()



