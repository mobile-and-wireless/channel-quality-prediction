from pathlib import Path
from itertools import count
import datetime,time

nofix = True
mIdx = 0
counter = count(mIdx)   
data_folder_meas = Path("data/meas/")
file_to_open = data_folder_meas / "measureAll.measWifi"
file_to_write = data_folder_meas / "mW.measWiFi"

#read input file
fin = open(file_to_open, "rt")
#read file contents to string
data = fin.read()
data = data.replace('SSID BSSID FREQUENCY LEVEL TIME','')
out = ''
dataL = data.split("\n")
lenData = len(dataL)
lineNumber = 0
pctComplete = 0
for line in dataL:
    if line != "":
        currentline = line.split(",")
        dForm = currentline[4]
        year = int(dForm[0:4])
        month = int(dForm[5:7])
        day = int(dForm[8:10])
        hour = int(dForm[11:13])
        minute = int(dForm[14:16])
        second = int(dForm[17:19])
        us = int(dForm[20:23])
        dt = datetime.datetime(year,month,day,hour,minute,second,us*1000)
        timOut = int(time.mktime(dt.timetuple())*1e3 + dt.microsecond/1e3)
       # print(dt)
        out = out + str(timOut) + '\n'
    
    if (lineNumber % int(lenData/10)) == 0:
        print('%d %% complete' % (pctComplete))
        pctComplete += 10
    lineNumber+=1
    
#close the input file
fin.close()

#open the input file in write mode
fin = open(file_to_write, "wt")
#overrite the input file with the resulting data
fin.write(out)
#close the file
fin.close()




