from pathlib import Path
import re
from itertools import count
import utils.MeasurementStrings as ms
import datetime,time
import json

def replace_strings(data):
    counter = count(mIdx)   
    #data = data.replace('{"measurements":\n\n}','')
    data = data.replace('}]}', '}\t\n        ]\t\n    }\t\n}')
    data = data.replace('[{"SSID"','        [\t\n            {"SSID"')
    data = data.replace('},{"SSID"','},\t\n            {"SSID"')
    data = data.replace('}	\n},{',',')
    data = data.replace('}\t\n},\n}{',',')
    data = data + '\t\n    }\t\n}'
    #data = re.sub(r'@', lambda x: str(next(counter)), data )

    return data

def proces_string(string,m_string):
    m_start = string.index('"m')
    string = string[m_start:]

    if m_string.string_empty_meas in string:
        m_empty = string.index(m_string.string_empty_meas)
        string_sandbox = string[m_empty-search_char_n_back:]
        if m_string.string_empty_meas_start in string_sandbox:
            m_start_empty = string_sandbox.index(m_string.string_empty_meas_start)
        else:
            m_start_empty = string_sandbox.index(m_string.string_empty_meas_start_empty)+1
  
        string = string[:m_empty-search_char_n_back+m_start_empty]

    string_end = string[len(string)-search_char_n_back:]
    m_end = string_end.index(']')
    string = string[:len(string)-search_char_n_back+m_end+1]
    
    return string

def time_convert_utc(string):
    #string = string.replace('            ','')
    data_json = json.loads(string)
    meas_data = data_json['measurements']
    lineNumber = 0  
    lenData = len(meas_data)
    pctComplete = 0
    for m in meas_data:
     
        for mm in range(len(meas_data[m])):
            line = meas_data[m][mm]['TIME']
            
           
            if line != "":

                year = int(line[0:4])
                month = int(line[5:7])
                day = int(line[8:10])
                hour = int(line[11:13])
                minute = int(line[14:16])
                second = int(line[17:19])
                us = int(line[20:23])
                dt = datetime.datetime(year,month,day,hour,minute,second,us*1000)
                timOut = int(time.mktime(dt.timetuple())*1e3 + dt.microsecond/1e3)
            # print(dt)
                #out = out + str(timOut) + '\n'
                
                data_json['measurements'][m][mm]['TIME'] = timOut

        if lenData >1:
            if (lineNumber % int(lenData/10)) == 0:
                print('%d %% complete' % (pctComplete))
                pctComplete += 10
            lineNumber+=1
        else:
                print('%d %% complete' % (100))
    return json.dumps(data_json, indent=4)



nofix = True
mIdx = 0
measCount = 0

data_folder_rooms = Path("data/rooms/")
data_folder_json = Path("data/json/")
data_folder_rooms_json = Path("data/roomjson/")
filesToProcess= {"p1measWifi","p2b.measmeasWifi","p3measWifi","p4correctmeasWifi","p5correctmeasWifi","p6measWifi","p7measWifi","p8measWifi"}

for file in filesToProcess:
    filenameIn = file + ".json"

    file_measureAll = data_folder_rooms / filenameIn
    m_string = ms.MeasurementStrings()

    #read input file
    fin = open(file_measureAll, "rt")
    #read file contents to string
    data = fin.read()

    data = replace_strings(data)
        
    # count number of measures and split it to separate files
    measureS_split = data.split('\"measurements\":')

    #close the input file
    fin.close()
    search_char_n_back = 50
    for mI in range(len(measureS_split)):
        if('"m' in measureS_split[mI]):
            string =proces_string(measureS_split[mI],m_string)
            string_file =  "Out"+ file + str(mI) +'.json'

            string_out = m_string.string_file_start + string + m_string.string_file_end

            string_out = time_convert_utc(string_out)
            file_mWjson = data_folder_rooms_json / string_file
            #open the input file in write mode
            fin = open(file_mWjson, "wt")
            #overrite the input file with the resulting data
            fin.write(string_out)
            #close the file
            fin.close()



