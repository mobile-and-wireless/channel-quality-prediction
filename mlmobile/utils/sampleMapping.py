from pathlib import Path


data_folder_txt = Path("data/txt/")

file_fMN= data_folder_txt / "mMtimes.txt"
file_fWN = data_folder_txt / "mWtimes.txt"
file_matching = data_folder_txt / "matching.txt"


fMN = open(file_fMN)
fWN = open(file_fWN)

timesM = fMN.read()
timesW = fWN.read()

timesM = timesM.split('\n')
timesW = timesW.split('\n')

matchingList = ''

indexSkip = -1
for lineM in timesM:
    indexW = 0
    if lineM != '':
        recM = lineM.split(':')
    
        timeM = int(recM[1])
        
        for lineW in timesW:
            if indexW >=indexSkip:
                recW = lineW.split(':')
                if int(recW[1])>=timeM:
                    # check if previous or current time is closer
                    tmp = timesW[indexW-1].split(':')
                    dPrev = timeM - int(tmp[1])
                    dNext = int(recW[1]) - timeM

                    if dPrev < dNext:
                        match = tmp[0]
                    else:
                        match = recM[0]
                    indexSkip = indexW -1
                    break     
                

            indexW += 1
        matchingList = matchingList + recM[0] + ':' + match + '\n'


#open the input file in write mode
fin = open(file_matching, "wt")
#overrite the input file with the resulting data
fin.write(matchingList)
#close the file
fin.close()
