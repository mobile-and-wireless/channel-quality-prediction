from itertools import count
from pathlib import Path
import json
import numpy as np

data_folder_txt = Path("data/txt/")
data_folder_json = Path("data/json/")
data_folder_npy = Path("data/npy/")

file_fmatch= data_folder_txt / "matching.txt"
file_fMids = data_folder_txt / "uniqueMIds.txt"
file_fWids = data_folder_txt / "uniqueWIds.txt"
file_fW = data_folder_json / "wifiInput.json"
file_fM = data_folder_json / "cellInput.json"
file_fout= data_folder_txt / "data.txt"
file_foutNp= data_folder_npy / "dataNp"

fmatch = open(file_fmatch)
fW = open(file_fW)
fM = open(file_fM) 

fMids = open(file_fMids)
fWids = open(file_fWids)

uMIds = fMids.read()
uWIds = fWids.read()

uMIdsList = uMIds.split('\n')
uMIdsList.pop()
uWIdsList = uWIds.split('\n')
uWIdsList.pop()

nMids = uMIds.count('\n')
nWids = uWIds.count('\n')
nIds =  nMids + nWids

idsListTemplate =  [0] * nIds
 

dataW = json.load(fW)
dataM = json.load(fM)


measW = dataW['measurements']
measM = dataM['measurements']

matching = fmatch.read()
nRecodrs = matching.count('\n')
arr = np.empty([nRecodrs,nIds])
matchRecord = matching.split('\n')
matchRecord.pop()
#open the input file in write mode
fout = open(file_fout,"wt")

dataOut = ''
row = 0
for match in matchRecord:
    indices = match.split(':')
    indexM = indices[0]
    indexW = indices[1]

    for mM in measM:
        if mM == indexM:

            for mW in measW:
                if mW == indexW:
                    idsList = idsListTemplate
                    # get cell measurements records and write to file
                    for rM in measM[mM]:
                        # find PCI in uMIdsList and add to idsList at correct location
                        position = uMIdsList.index(rM['PCI'])
                        idsList[position] = int(rM['RSPR'])
                        arr[row,position ] = idsList[position]
                    for rW in measW[mW]:
                         # find BSSID in uWIdsList and add to idsList at correct location
                        # find PCI in uMIdsList and add to idsList at correct location
                        position = uWIdsList.index(rW['BSSID'])
                        idsList[position+nMids] = int(rW['LEVEL'])
                        arr[row,position+nMids] = idsList[position+nMids]

    writeIndex = 0 
    row +=1                 
    # write to file 
    for id in idsList:
        #overrite the input file with the resulting data
        fout.write(str(id))
        if writeIndex != (nIds-1):
            fout.write(',')
        else:
            fout.write('\n')  
        writeIndex += 1  

#close the file
fout.close()
np.save(file_foutNp,arr)
