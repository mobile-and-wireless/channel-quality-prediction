import json
from pathlib import Path


fileNet = "mlNet.net"
data_folder_trained_net = Path("data/trainedNet/")

filePath = data_folder_trained_net / fileNet


stringNetName = "mlDemo"
predictCell = '50501'
predictors = ['50504','50505','50507']



net = {
    "net-name" : stringNetName,
    "predict-cell" :predictCell,
    "predictor-cells" : predictors
}

my_json = json.dumps(net)

#open the input file in write mode
fin = open(filePath, "wt")
#overrite the input file with the resulting data
fin.write(my_json)
#close the file
fin.close()
