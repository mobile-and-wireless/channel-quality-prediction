import os
import pathlib as Path
import numpy as np

data_folder_txt = Path("data/txt/")
data_folder_npy = Path("data/npy/")

file_data = data_folder_npy / "dataFilter.npy"

fileIn  = "dataReal/20211124meas.txt" #sys.argv[1]
startString = "Serving"
stringCellId = "Cell Id: "
stringRsrp = " RSRP: "
lineNumberStart = 0 #int(sys.argv[2])
lineNumber = 0
filename =  "dataClean/processed.txt" #"prediction/pred.txt"
if(os.path.isfile(filename) == False):
    f = open(filename, "x")  
    f.write("")
    f.close() 

  

f = open(filename, "a")  


# Process the file line-by-line
fpIn = open(file_data,'r')
Lines = fpIn.readlines()

for line in Lines:
    rsrpVect = ["NaN","NaN","NaN","NaN","NaN","NaN"]
    lineNumber += 1
    
    startStringIndex = line.find(startString)
    startStringIndex += len(startString)
    measurements = line[startStringIndex:]
    measurement = measurements.split(";")

    for meas in measurement:

        comIdx = meas.find(",",1)
        ceidStart= len(stringCellId) +1
        cellId = meas[ceidStart:comIdx] 
        rsrpStart= len(stringRsrp) +1
        rsrp = meas[comIdx + rsrpStart:]
        print("cellId "  + cellId + " rsrp: " + rsrp)


        if cellId == '0':
            rsrpVect[0] = rsrp
        elif cellId == '61':
            rsrpVect[1] = rsrp
        elif cellId == '62':
            rsrpVect[2] = rsrp
        elif cellId == '63':
            rsrpVect[3] = rsrp
        elif cellId == '64':
            rsrpVect[4] = rsrp
        elif cellId == '65':
            rsrpVect[5] = rsrp

    pred_write =rsrpVect[0] + "," + rsrpVect[1] + "," + rsrpVect[2] + "," + rsrpVect[3] + "," + rsrpVect[4] +"," + rsrpVect[5] +"\n" 
    f.write(pred_write)

f.close()   


   
        
        