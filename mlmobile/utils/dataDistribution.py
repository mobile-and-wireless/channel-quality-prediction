from pathlib import Path

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

# Path to measured data
data_out_dir  = Path("mlmobile/data/txt") 
data_out_file = "dataOMFilterAugment2.txt"#"dataMeas2FilterAugment.txt"
file_name     = data_out_dir / data_out_file

# Path to simulated data
sim_out_dir   = Path("ml-mobile-simulation/data/txt")
sim_out_file  = "data5shadowint.txt"
file_name_sim = sim_out_dir / sim_out_file  

# Load data to numpy arrays using pandas
df_data    = pd.read_csv(file_name, delimiter=',', usecols=[0,1,2,3,4,5])
df_sim     = pd.read_csv(file_name_sim, delimiter=',')
data_array = pd.DataFrame(df_data).to_numpy() 
sim_array  = pd.DataFrame(df_sim).to_numpy()

plot_out_dir   = Path("mlmobile/data/plot/png")
plot_sim_file  = "sim_hist.png"
plot_data_file = "data_hist.png"
path_sim       = plot_out_dir / plot_sim_file
path_data      = plot_out_dir / plot_data_file 

# Histogram
plt.figure(1)
sns.histplot(data_array[:,0], bins='auto', kde=True)
plt.xlabel("Measured RSRP [dB]") 
plt.ylabel("Density [-]") 
plt.legend() 
plt.title("RSRP Distribution of Measured Data")
plt.savefig(path_data, dpi=300)
plt.show()

plt.figure(2)
sns.histplot(sim_array[:,0], bins='auto', kde=True)
plt.xlabel("Simulated RSRP [dB]") 
plt.ylabel("Density [-]") 
plt.legend() 
plt.title("RSRP Distribution of Simulated Data")
plt.savefig(path_sim, dpi=300)
plt.show()
    