from pathlib import Path
import numpy as np
import os

data_folder_txt = Path("data/txt/")
data_folder_npy = Path("data/npy/")

abs_path = os.getcwd()

filename = "dataMeas2FilterAugment.txt" 
filenameOut = "dataMeas2FilterAugmentcells.txt" 

file_in_path= abs_path / data_folder_txt /filename
file_out_path= abs_path / data_folder_txt /filenameOut

#file_in_path_fp = open(file_in_path, "rt")
#file_out_path_fp = open(file_out_path,"wt") 


in_data = np.loadtxt( file_in_path,delimiter = ',')
out_data = np.delete(in_data, [6, 7, 8, 9, 10, 11,], 1)
np.savetxt(file_out_path, out_data, delimiter=',')
#np.save(file_out_path, out_data)