


def setup_data(data):

    data.pop('cell0')


def normalize_input_data(data):
    #normalize input data almost <-1,1> 
    return (data - 100)/10


import sys
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd # working with sheets
import seaborn as sns # advanced vizualization
from pathlib import Path # working with directories
import utils.Model as model

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras # API for working with models
from keras.regularizers import l1 # regulation for overlearning (big arguments)
from keras.regularizers import l2
from utils.dnnToJSON import export_dnn, export_dnn_results

# Make numpy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)

#set deterministic behavior for numpy and tensorflow
fixed_seed = True
if fixed_seed:
    np.random.seed(1)
    tf.random.set_seed(2)

plotComparison = True
plotHistory = False
normalize = False
rsrp_threshold = -110
train_percentage = 0.7

#creating new model, set the columns which will be dropped?
dnn_mdl = model.Model()
dnn_mdl.columns_drop_wifi =[] #[*range(0,44)]
dnn_mdl.columns_drop_mobile = [] # [*range(6,39)]
dnn_mdl.number_inputs_train = 53

#path to folders
data_folder_npy = Path("data/npy/")
data_folder_txt = Path("data/txt/")
data_folder_csv = Path("data/csv/")
data_folder_mdl = Path("data/mdl/")

filename = "dmod1.txt" 
filename_out_append = 'tl5zero22'
file_data_header = data_folder_txt / "dataTest_header.txt"
file_data_combo = data_folder_txt /"comboSelectedRemovalOMAdam2.txt"
file_data_comboCompare = data_folder_txt /"comboSelectedRemovalCompareOMAdam2.txt"
# file_save_model = data_folder_mdl /"mdlShadowZerox2AdamFinalTransfer.tf"
file_save_model = data_folder_mdl /"modelTransfer.tf"

if len(sys.argv) > 1:
    file_datamod=data_folder_txt / sys.argv[1] # if specified by input
else:
    # file_datamod= data_folder_txt / "data5intAdam.txt" # default
    file_datamod= data_folder_txt / "dataTest.txt" # default


#read list of cells and tranfer it to array (headers for next CSV file)
f = open(file_data_header)
data_Np_Header = f.read()
headers = data_Np_Header.split(';')
headers.pop() 

raw_dataset = pd.read_csv(file_datamod, names=headers,
                        na_values='?', comment='\t',
                        sep=',', skipinitialspace=False)


dnn_mdl.dataset = abs(raw_dataset.copy())

dnn_mdl.number_inputs_wifi = dnn_mdl.dataset.shape[1]
setup_data(raw_dataset)


load_mdl  = tf.keras.models.load_model(file_save_model)

#mean absolute error
predicted = load_mdl.predict(abs(raw_dataset))
print(f"predicted data of cell 0: {predicted}")


