from pyexpat.errors import XML_ERROR_UNDECLARING_PREFIX


class room:
    def __init__(self, xcor, ycor, width = 0, length = 0,room_name = '', wall_attenutations = [20,20,20,20],wall_thicknesses = [50,50,50,50]):
        """
        xcor = [x1, x2] ; ycor = [y1, y2] 
        x1, y1 *-----* x2, y1
               I     I
               I     I 
        x1, y2 *-----* x2, y2

        convention gizmondo at lower left corner, i.e. x = 0, y = 0 
        """
        self.xcor   = xcor
        self.ycor   = ycor
        self.doors = []
        self.length = length # this must be defined to be able to call floor.wall()
        self.width  = width
        self.room_name = room_name
        self.inner_walls = []
        self.wall_attenutations = wall_attenutations # wall order considering y axis in postive numbers ot direct towards nortth: west, north, east, south
        self.wall_thicknesses = wall_thicknesses # wall order considering y axis in postive numbers ot direct towards nortth: west, north, east, south


    def add_door(self, xstart, ystart, horizontal,door_w):
        if horizontal:
           self.doors.extend([ [xstart, xstart+door_w], [ystart, ystart] ] )
        elif not horizontal:
            self.doors.extend([ [xstart, xstart], [ystart, ystart+door_w] ])
        #end if      

    def add_inner_wall(self, x_start, y_start,x_end,y_end, attenuation = 0, thickness = 0):
        """ Defines and draws walls inside the room """
        self.inner_walls.extend(wall(x_start, y_start,x_end,y_end, attenuation , thickness ))
        return 


class wall:
    def __init__(self, x_start, y_start,x_end,y_end, attenuation = 0, thickness = 0):
        self.x_start = x_start
        self.y_start = y_start
        self.x_end = x_end
        self.y_end = y_end
        self.attenuation = attenuation
        self.thickness = thickness
