import numpy as np
class eNB:
  
    def __init__(self, xcor, ycor, room = None, room_id = -1, tx_power_db = 23, height = 2000, frequency = 2.6, technology = '4G', mcc = 0, mnc = 0, cellid = 0):
        self.location = [xcor,ycor,height]
        self.height = height
        self.room = room
        self.room_id = room_id
        self.tx_power_db = tx_power_db
        self.frequency = frequency
        self.technology = technology
        self.mcc = mcc
        self.mnc = mnc
        self.cellid = cellid 
        # antenana parameters

