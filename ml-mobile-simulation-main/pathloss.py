import numpy as np
import scipy
from scipy.interpolate import CubicSpline
from classes import settings, user  

def shadowing(mu, sigma, area_x, area_y, expanding_factor, exp_factor_time, number_of_steps):
    """
     expanding_factor=10;
     exp_factor_time=10;
     mi1=0; [dB]
     sigm1=10; [dB]
     number_of_steps=500;
     Nx=890;
     Ny=190;
    """
    Nx=area_x // 10   # lower memory consumption, interpolate other positions
    Ny=area_y // 10   # lower memory consumption, interpolate other positions

    Nxexpand = Nx*expanding_factor
    Nyexpand = Ny*expanding_factor
    number_of_steps = number_of_steps // exp_factor_time
    s  = (Nxexpand,Nyexpand,number_of_steps+1)
    Vi = np.zeros(s, dtype=float)
    Ls = mu + sigma*np.random.randn(Nx, Ny, number_of_steps+1);
    
    for l in range(number_of_steps):
        Vq = scipy.interpolate.interp2d(Ls[:,:,1], 4)
        Vq = Vq*sigma/np.max(np.max(Vq))
        Vi[:,:,l] = Vq[1:Nxexpand, 1:Nyexpand]
    # end for

    t0 = np.arange(1, number_of_steps+2)
    t01 = [1 + x/exp_factor_time for x in range(number_of_steps+1)]
    t01[2] = []

    x_interp = np.zeros(area_x, area_x, number_of_steps*exp_factor_time)
    n_parts = area_x // 100

    for part in range(n_parts):
        x_interp[((part-1)*100+1):part*100,:,:] = CubicSpline(t0, Vi[((part-1)*100+1):part*100,:,:])
    # end for

    return x_interp


def path_loss(settings, user, los):

    return
    