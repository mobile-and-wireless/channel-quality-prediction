from array import *
import matplotlib.pyplot as plt
import numpy as np
from utils.channel import channel
import utils.floorPlan as fp
import utils.room as room
import utils.eNB as eNB
import utils.user as user    
import utils.simulation as simulation
from pathlib import Path
import os

def create_rooms(fplan):
    """ Specify coordinates of verticies 
        connected by walls to create rooms """
    a4_503a= room.room(xcor = 0, ycor =  0, width = 5650, length = 4820,room_name = 'A4-503A',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,210,170,200])
    a4_503a.add_door( xstart = 5650, ystart = 2250, horizontal = 0,door_w = 970)
    a4_503a.add_door( xstart = 4230, ystart = 4820, horizontal = 1,door_w = 800)
    fplan.add_room(a4_503a.room_name,a4_503a)

    a4_503b= room.room(xcor = 0, ycor =  4820, width = 5650, length = 2450,room_name = 'A4-503B',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,100,170,210])
    a4_503b.add_door( xstart = 5650, ystart = 5645, horizontal = 0,door_w = 970)
    a4_503b.add_door( xstart = 4230, ystart = 4820, horizontal = 1,door_w = 800)
    fplan.add_room(a4_503b.room_name,a4_503b)

    a4_504a= room.room(xcor = 0, ycor =  7480, width = 5650, length = 2450,room_name = 'A4-504A',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,210,170,100])
    a4_504a.add_door( xstart = 4230, ystart = 9930, horizontal = 1,door_w = 800)
    fplan.add_room(a4_504a.room_name,a4_504a)

    a4_504b= room.room(xcor = 0, ycor =  9930, width = 5650, length = 4770,room_name = 'A4-504B',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,210,170,210])
    a4_504b.add_door( xstart = 4230, ystart = 9930, horizontal = 1,door_w = 800)
    a4_504b.add_door( xstart = 5650, ystart = 10830, horizontal = 0,door_w = 970)
    fplan.add_room(a4_504b.room_name,a4_504b)

    a4_505kitchen= room.room(xcor = 0, ycor =  14700, width = 5000, length = 2450,room_name = 'A4-505kitchen',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,50,50,200])
    a4_505kitchen.add_door( xstart = 5000, ystart = 15500, horizontal = 0,door_w = 840)
    fplan.add_room(a4_505kitchen.room_name,a4_505kitchen)

    a4_505d= room.room(xcor = 0, ycor =  17150, width = 5000, length = 12400,room_name = 'A4-505D',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,500,200,50])
    a4_505d.add_door( xstart = 5000, ystart = 17150, horizontal = 0,door_w = 1080)
    fplan.add_room(a4_505d.room_name,a4_505d)

    a4_505b= room.room(xcor = 5650, ycor =  19500, width = 2900, length = 10000,room_name = 'A4-505B',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,500,200,50])
    a4_505b.add_door( xstart = 5650, ystart = 19500, horizontal = 1,door_w = 970)
    fplan.add_room(a4_505b.room_name,a4_505b)

    a4_505c= room.room(xcor = 8550, ycor =  19500, width = 4200, length = 10000,room_name = 'A4-505C',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,500,200,50])
    a4_505c.add_door( xstart = 8550, ystart = 19500, horizontal = 1,door_w = 800)
    fplan.add_room(a4_505c.room_name,a4_505c)

    a4_505cside= room.room(xcor = 12750, ycor =  19500, width = 750, length = 12400,room_name = 'A4-505Cside',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,500,200,50])
    a4_505cside.add_door( xstart = 12750, ystart = 19500, horizontal = 0,door_w = 1080)
    fplan.add_room(a4_505cside.room_name,a4_505cside)

    a4_505a= room.room(xcor = 6030, ycor =  14700, width = 5000, length = 12400,room_name = 'A4-505A',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,500,200,50])
    a4_505a.add_door( xstart = 6030, ystart = 14700, horizontal = 0,door_w = 1080)
    fplan.add_room(a4_505a.room_name,a4_505a)

    a4_505central= room.room(xcor = 4980, ycor =  14700, width = 4300, length = 4800,room_name = 'A4-505central',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,500,200,50])
    a4_505central.add_door( xstart = 5000, ystart = 15500, horizontal = 0,door_w = 840)
    a4_505central.add_door( xstart = 5000, ystart = 17150, horizontal = 0,door_w = 1080)
    a4_505central.add_door( xstart = 5650, ystart = 19500, horizontal = 1,door_w = 970)
    a4_505central.add_door( xstart = 8550, ystart = 19500, horizontal = 1,door_w = 800)
    a4_505central.add_door( xstart = 6030, ystart = 14700, horizontal = 0,door_w = 1080)
    fplan.add_room(a4_505central.room_name,a4_505central)

    a4_506a= room.room(xcor = 7650, ycor =  9930, width = 5650, length = 4770,room_name = 'A4-506A',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,500,200,50])
    a4_506a.add_door( xstart = 7650, ystart = 10830, horizontal = 0,door_w = 970)
    fplan.add_room(a4_506a.room_name,a4_506a)

    a4_506b= room.room(xcor = 7650, ycor =  4820, width = 5650, length = 1900,room_name = 'A4-506B',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,210,170,210])
    a4_506b.add_door( xstart = 7650, ystart = 7400, horizontal = 0,door_w = 970)
    fplan.add_room(a4_506b.room_name,a4_506b)
    
    a4_wc= room.room(xcor = 7650, ycor =  0, width = 5650, length = 4820,room_name = 'A4-WC',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,500,200,50])
    fplan.add_room(a4_wc.room_name,a4_wc)

    a4_corridor= room.room(xcor = 5650, ycor =  0, width = 2000, length = 14700,room_name = 'A4-corridor',wall_attenutations = [50,50,50,50],wall_thicknesses = [100,500,200,50])
    a4_corridor.add_door( xstart = 5900, ystart = 14550, horizontal = 0,door_w = 1500)
    fplan.add_room(a4_corridor.room_name,a4_corridor)
    
    """ Add door """ 
    #a4503.set_door(535, 2380, 0)  
    return fplan
    #return [a4_503, a4504, a4504b, a4505a, a4505b, a4505c, a4505d, a4506, a4506b, a4507_9, kitchen]

def update_adjacency(fplan):
    fplan.add_room_adjacency('A4-503A',[{'room': "A4-503B",'attenuation': 10},{'room': "A4-corridor",'attenuation': 20}])
    fplan.add_room_adjacency('A4-503B',[{'room': "A4-503A",'attenuation': 10},{'room': "A4-corridor",'attenuation': 20},{'room': "A4-504A",'attenuation': 15}])
    fplan.add_room_adjacency('A4-504A',[{'room': "A4-504B",'attenuation': 10},{'room': "A4-corridor",'attenuation': 20},{'room': "A4-503B",'attenuation': 15}])
    fplan.add_room_adjacency('A4-504B',[{'room': "A4-504A",'attenuation': 10},{'room': "A4-corridor",'attenuation': 20},{'room': "A4-505kitchen",'attenuation': 20}])
    fplan.add_room_adjacency('A4-505kitchen',[{'room': "A4-504A",'attenuation': 20},{'room': "A4-505central",'attenuation': 15},{'room': "A4-505D",'attenuation': 15}])
    fplan.add_room_adjacency('A4-505D',[{'room': "A4-505B",'attenuation': 20},{'room': "A4-505central",'attenuation': 15},{'room': "A4-505kitchen",'attenuation': 15}])
    fplan.add_room_adjacency('A4-505B',[{'room': "A4-505D",'attenuation': 20},{'room': "A4-505central",'attenuation': 15},{'room': "A4-505C",'attenuation': 15}])
    fplan.add_room_adjacency('A4-505C',[{'room': "A4-505A",'attenuation': 15},{'room': "A4-505central",'attenuation': 10},{'room': "A4-505B",'attenuation': 15},{'room': "A4-505Cside",'attenuation': 15}])
    fplan.add_room_adjacency('A4-505Cside',[{'room': "A4-505A",'attenuation': 15},{'room': "A4-505C",'attenuation': 15}])
    fplan.add_room_adjacency('A4-505A',[{'room': "A4-505C",'attenuation': 15},{'room': "A4-505Cside",'attenuation': 15},{'room': "A4-505central",'attenuation': 10},{'room': "A4-506A",'attenuation': 15}])
    fplan.add_room_adjacency('A4-506A',[{'room': "A4-506B",'attenuation': 15},{'room': "A4-corridor",'attenuation': 20},{'room': "A4-505A",'attenuation': 15}])
    fplan.add_room_adjacency('A4-506B',[{'room': "A4-506A",'attenuation': 15},{'room': "A4-corridor",'attenuation': 20},{'room': "A4-WC",'attenuation': 20}])
    fplan.add_room_adjacency('A4-WC',[{'room': "A4-506B",'attenuation': 20},{'room': "A4-corridor",'attenuation': 20}])
    fplan.add_room_adjacency('A4-corridor',[{'room': "A4-506B",'attenuation': 20},{'room': "A4-506A",'attenuation': 20},{'room': "A4-504B",'attenuation': 20},{'room': "A4-504A",'attenuation': 20},{'room': "A4-503B",'attenuation': 20},{'room': "A4-503A",'attenuation': 20},{'room': "A4-WC",'attenuation': 20},{'room': "A4-505central",'attenuation': 10}])
    fplan.add_room_adjacency('A4-505central',[{'room': "A4-505kitchen",'attenuation': 15},{'room': "A4-505D",'attenuation': 15},{'room': "A4-505B",'attenuation': 15},{'room': "A4-505C",'attenuation': 10},{'room': "A4-505A",'attenuation': 10},{'room': "A4-corridor",'attenuation': 10}])

    return fplan
""" Create floor and rooms, place eNB and user """
n_drops = 5
filename = 'data1.txt'
filename_header = filename[0:len(filename)-4] + '_header.txt'
filename_integer = 'data1int.txt'
filename_integer_header = filename_integer[0:len(filename_integer)-4] + '_header.txt'

sim = simulation.simulation(13500,32000)

fplan = create_rooms(sim.floorPlan)
fplan = update_adjacency(fplan)
sim.floorPlan.determine_room_to_room_attenuation()
sim.floorPlan.room_adjacency_path_pre_calculate()
sim.set_floorPlan(fplan)
sim.add_eNB(0,eNB.eNB(xcor = 8550, ycor = 17150, room = 'A4-505central', room_id = sim.floorPlan.get_room_index_from_room_name('A4-505central'), tx_power_db = 23, height = 1, frequency = 2.6, technology = '4G', mcc = 505, mnc = 1, cellid = 1))
sim.add_eNB(1,eNB.eNB(xcor = 5650, ycor = 10930, room = 'A4-504B', room_id = sim.floorPlan.get_room_index_from_room_name('A4-504B'),tx_power_db = 23, height = 2, frequency = 2.6, technology = '4G', mcc = 505, mnc = 2, cellid = 2))
sim.add_eNB(2,eNB.eNB(xcor = 50, ycor = 15500, room = 'A4-505kitchen', room_id = sim.floorPlan.get_room_index_from_room_name('A4-505kitchen'),tx_power_db = 23, height = 1, frequency = 2.6, technology = '4G', mcc = 505, mnc = 3, cellid = 3))
sim.add_eNB(3,eNB.eNB(xcor = 7600, ycor = 5645, room = 'A4-corridor', room_id = sim.floorPlan.get_room_index_from_room_name('A4-corridor'),tx_power_db = 23, height = 1, frequency = 2.6, technology = '4G', mcc = 505, mnc = 4, cellid = 4))
sim.add_eNB(4,eNB.eNB(xcor = 11350, ycor = 25100, room = 'A4-505C',room_id = sim.floorPlan.get_room_index_from_room_name('A4-505C'), tx_power_db = 23, height = 1, frequency = 2.6, technology = '4G', mcc = 505, mnc = 5, cellid = 5))
sim.add_eNB(5,eNB.eNB(xcor = 2100, ycor = 24100, room = 'A4-505d', room_id = sim.floorPlan.get_room_index_from_room_name('A4-505d'),tx_power_db = 23, height = 1, frequency = 2.6, technology = '4G', mcc = 505, mnc = 6, cellid = 6))


sim.gen_user_loc()
#room_name = sim.floorPlan.get_room_from_location([6550, 16150])

#sim.add_user(0,user.user(xcor = 6550, ycor = 16150, room = room_name, room_id = sim.floorPlan.get_room_index_from_room_name(room_name)))

print('initialized')
drop = 0
while drop <= n_drops:
    # determine rsrp
    ch = channel(model = '3GPPindoor',room_scale = 1000, step_interp=0.1)
    ch.set_wall_room_attenuation(sim.floorPlan.attenuation_between_rooms)
    ch.shadowing(mu=0,sigma = 3,step = 1,room = sim.floorPlan.sim_room)
    for uidx in range(0,len(sim.users)):
        for enbidx in range(0,len(sim.eNBs)):
            sim.users[uidx].distances[enbidx] = ch.dist(loc1 = sim.eNBs[enbidx].location,loc2 = sim.users[uidx].location)
            if sim.users[uidx].room == sim.eNBs[enbidx].room:
                sim.users[uidx].LOS[enbidx] = 1
            else:
                sim.users[uidx].LOS[enbidx] = 0
            rm1 = sim.users[uidx].room_id
            rm2 = sim.eNBs[enbidx].room_id

            sim.users[uidx].rsrp[enbidx] = sim.eNBs[enbidx].tx_power_db - ch.pathloss(distance = sim.users[uidx].distances[enbidx], los = sim.users[uidx].LOS[enbidx],rm1 = rm1, rm2 = rm2,ue_location=sim.users[uidx].location) 

    data_folder_txt = Path("data/txt/")
    if not os.path.exists(data_folder_txt):
        os.makedirs(data_folder_txt)
    path = data_folder_txt / filename
    path_header = data_folder_txt / filename_header
    path_integer = data_folder_txt / filename_integer
    path_integer_header = data_folder_txt / filename_integer_header

    sim.write_to_file(path,path_header,path_integer,path_integer_header,drop)
    drop += 1

print('data generated')

