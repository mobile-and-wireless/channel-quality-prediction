import utils.floorPlan as floorPlan
import utils.floorPlan as floorPlan

import utils.user as user
class simulation():
    def __init__(self,width, length,user_location_step=100):
        self.floorPlan = floorPlan.floorPlan(length, width)
        self.eNBs = {}
        self.users = {}
        self.user_location_step = user_location_step
        
    def set_floorPlan(self,floorPlan):
        self.floorPlan = floorPlan

    def add_eNB(self,enb_id,enb):
        self.eNBs[enb_id] = enb

    def add_user(self,user_id,user):
        self.users[user_id] = user
        self.users[user_id].room = self.get_room_from_location(user.location)

    def gen_users(self):
        while True:
            for x_pos in range(0,self.floorPlan.width):
                for y_pos in range (0,self.floorPlan.length):
                    pass

    def get_room_from_location(self,location):
        return self.floorPlan.get_room_from_location(location)

    def gen_user_loc(self):
        ue_location = self.floorPlan.generate_user_locations(self.user_location_step)
        user_id = len(self.users)
        for x_cor,y_cor in ue_location:
            room_name = self.floorPlan.get_room_from_location([x_cor, y_cor])
            self.add_user(user_id,user.user(xcor = x_cor, ycor = y_cor, room = room_name, room_id = self.floorPlan.get_room_index_from_room_name(room_name)))
            user_id += 1

    def write_to_file(self,file_path,file_path_header,file_path_integer = None,file_path_integer_header=None,drop = 0):
        if drop > 0:
            file_option = 'a'
        else:
            file_option = 'wt'

        fin = open(file_path, file_option)
    
        if file_path_integer:
            fin_int = open(file_path_integer, file_option)

        for ue in self.users:
            rsrp_id = 1
            for rsrp in self.users[ue].rsrp: 
                if rsrp_id< len(self.eNBs):
                    fin.write(str(self.users[ue].rsrp[rsrp]) + ',')
                    fin_int.write(str(round(self.users[ue].rsrp[rsrp])) + ',')
                else:
                    fin.write(str(self.users[ue].rsrp[rsrp]) )
                    fin_int.write(str(round(self.users[ue].rsrp[rsrp])))

                rsrp_id +=1
                #close the file
            fin.write('\n')
            fin_int.write('\n')
        fin.close()
        fin_int.close()


        if drop == 0: # jsut for the first drop
            finh = open(file_path_header, "wt")
            for enb in self.eNBs:
                finh.write('cell' + str(enb) + ';')
            finh.close()


            if file_path_integer_header:
                fin_inth = open(file_path_integer_header, "wt")
                for enb in self.eNBs:
                    fin_inth.write('cell' + str(enb) + ';')
                fin_inth.close()

