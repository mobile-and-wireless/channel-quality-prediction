from scipy import interpolate
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib.ticker import LinearLocator

def shadowing(mu, sigma, step, step_interp, room):
        # room wider than longer -> square width x width
        if room.width > room.length:
             x_min = room.xcor
             x_max = room.xcor+room.width
             y_min = room.ycor
             y_max = room.ycor+room.width
        # room longer than wider -> square length x length
        elif room.width <= room.length:
            x_min = room.xcor
            x_max = room.xcor+room.length
            y_min = room.ycor
            y_max = room.ycor+room.length

        x = np.arange(x_min,x_max,step)
        y = np.arange(y_min,y_max,step)

        xx, yy = np.meshgrid(x, y)

        # generate shadowing with given mu and sigma
        mu, sigma = 0, 3 # mean and standard deviation
        z = np.random.normal(mu, sigma, [x_max-x_min,y_max-y_min])

        f = interpolate.interp2d(x, y, z, kind='cubic')

        # determine shadowing values on interpolated grid (znew)
        xnew = np.arange(x_min, x_max, step_interp)
        ynew = np.arange(y_min, y_max, step_interp)

        znew = f(xnew, ynew)

        return znew[0:(room.width//step_interp), 0:(room.length//step_interp)]