import numpy as np


class settings:
    f_carrier      = 2.75E+9 # [Hz]
    wall_atten     = 100     # [dB] wall attenuation
    wall_thickness = 5       # [cm]
    floor_width    = 1420    # [cm]
    floor_length   = 3000    # [cm]
    tile_width     = 60      # [cm]
    tile_length    = 60      # [cm]   

class simulation:
    def __init__(self):
        pass

class floorPlan(simulation):
    grid   = np.zeros((settings.floor_length, settings.floor_width))
    
    def create(self, rooms):
        """     Create floor  
        Draw all the walls to distinguish rooms """
        for r in rooms:
                self.wall(r.xcor[0], r.xcor[0], r.ycor[0], r.ycor[1])
                self.wall(r.xcor[0], r.xcor[1], r.ycor[1], r.ycor[1])
                self.wall(r.xcor[0], r.xcor[1], r.ycor[0], r.ycor[0])
                self.wall(r.xcor[1], r.xcor[1], r.ycor[0], r.ycor[1])             

    def wall(self, xstart, xstop, ystart, ystop):
        """ Places a wall on the floor to simulate NLOS
            self.grid[y][x] (the plot is inverted) """
        if ystart == ystop: # horizontal wall
            for x in range(xstart, xstop):
                for i in range(-2,3):
                    if (ystart+i) < settings.floor_length:
                        self.grid[ystart+i][x] = 100
                    #end if    
                #end for
            #end for
        elif xstart == xstop: # vertical wall
            for y in range(ystart, ystop):
                for i in range(-2,3):
                    if (xstart+i) < settings.floor_width:
                        self.grid[y][xstart+i] = 100
                    #end if    
                #end for
            #end for
        #end if


class room(floorPlan):
    def __init__(self, xcor, ycor):
        """
        xcor = [x1, x2] ; ycor = [y1, y2] 
        x1, y1 *-----* x2, y1
               I     I
               I     I 
        x1, y2 *-----* x2, y2
        """
        self.xcor   = xcor
        self.ycor   = ycor
        self.door_w = 80


    def set_door(self, xstart, ystart, horizontal):
        if horizontal:
           self.door = [ [xstart, xstart+self.door_w], [ystart, ystart] ] 
        elif not horizontal:
            self.door = [ [xstart, xstart], [ystart, ystart+self.door_w] ]
        #end if      

    def inner_wall(self, xcor, ycor):
        """ Defines and draws walls inside the room """
        self.iwall = True
        self.iwall_x = xcor
        self.iwall_y = ycor
        super().wall(xcor[0], xcor[1], ycor[0], ycor[1])


class eNB:
    room_obj = []
    def __init__(self, xcor, ycor):
        self.xcor = xcor
        self.ycor = ycor

    def place(self, rooms):
        for rm in rooms:
            flag = True
            if self.is_in_room(rm):
                flag = self.is_in_wall(rm)
            
            if (not flag):
                self.room_obj = rm
            elif (flag == '->'):
                self.xcor += settings.wall_thickness
            elif (flag == '<-'):
                self.xcor -= settings.wall_thickness
            elif (flag == '^'):
                self.ycor += settings.wall_thickness
            elif (flag == 'v'):
                self.xcor -= settings.wall_thickness
            #end if    

    def dist(self, xuser, yuser):
        """ Calculates euclidean distance from eNB to user """
        return np.sqrt( (xuser-self.xcor)**2 + (yuser-self.ycor)**2 )

    def is_in_room(self, room):
        if ((self.xcor >= room.xcor[0]) and (self.xcor <= room.xcor[1])):
            if ((self.ycor >= room.ycor[0]) and (self.ycor <= room.ycor[1])):
                return True
            #end if
        #end if
        return False   
    
    def is_in_wall(self, room):
        if (self.xcor == room.xcor[0]):
            return '->'
        elif (self.xcor == room.xcor[1]):
            return '<-'
        elif (self.ycor == room.ycor[0]):
            return 'v'
        elif (self.ycor == room.ycor[1]):
            return '^'            
        #end if
        return False         

class user:
    def __init__(self, xcor, ycor):
        self.xcor = xcor
        self.ycor = ycor

    def is_in_room(self, room):
        if ((self.xcor >= room.xcor[0]) and (self.xcor <= room.xcor[1])):
            if ((self.ycor >= room.ycor[0]) and (self.ycor <= room.ycor[1])):
                return True
            #end if
        #end if
        return False   
    
    def is_in_wall(self, room):
        if (self.xcor == room.xcor[0]):
            return '->'
        elif (self.xcor == room.xcor[1]):
            return '<-'
        elif (self.ycor == room.ycor[0]):
            return 'v'
        elif (self.ycor == room.ycor[1]):
            return '^'            
        #end if
        return False