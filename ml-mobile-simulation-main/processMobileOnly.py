from pathlib import Path
import re
from itertools import count
import utils_local.MeasurementStrings as ms
import json
import datetime,time
import numpy as np

def replace_strings_mobile(data, mIdx):
    data = data.replace('},{"technology"','},\n\t\t\t{"technology"')
    data = data.replace('}]},{"m','}],\n\t\t"m')
    data = data + '\t\n    }\t\n}'
    #mIdx = 0
    counter = count(mIdx)   
  
    data = re.sub(r'm[0-9]+','m@',data)
    data = re.sub(r'@', lambda x: str(next(counter)), data )   
    return data, next(counter)


def replace_strings_wifi(data):
    data = data.replace('}]}', '}\t\n        ]\t\n    }\t\n}')
    data = data.replace('[{"SSID"','        [\t\n            {"SSID"')
    data = data.replace('},{"SSID"','},\t\n            {"SSID"')
    data = data.replace('}	\n},{',',')
    data = data.replace('}\t\n},\n}{',',')
    data = data + '\t\n    }\t\n}'

    return data


def proces_string_mobile(string,m_string):
    search_char_n_back = 50

    m_start = string.index('"m')
    string = string[m_start:]

    if m_string.string_empty_meas in string:
        m_empty = string.index(m_string.string_empty_meas)
        string_sandbox = string[m_empty-search_char_n_back:]
        m_start_empty = string_sandbox.index(m_string.string_empty_meas_start)
        string = string[:m_empty-search_char_n_back+m_start_empty]

    string_end = string[len(string)-search_char_n_back:]
    m_end = string_end.index(']')
    string = string[:len(string)-search_char_n_back+m_end+1]
  
    return string

def proces_string_wifi(string,m_string):
    search_char_n_back = 50

    m_start = string.index('"m')
    string = string[m_start:]

    if m_string.string_empty_meas in string:
        m_empty = string.index(m_string.string_empty_meas)
        string_sandbox = string[m_empty-search_char_n_back:]
        if m_string.string_empty_meas_start in string_sandbox:
            m_start_empty = string_sandbox.index(m_string.string_empty_meas_start)
        else:
            m_start_empty = string_sandbox.index(m_string.string_empty_meas_start_empty)+1
  
        string = string[:m_empty-search_char_n_back+m_start_empty]

    string_end = string[len(string)-search_char_n_back:]
    m_end = string_end.index(']')
    string = string[:len(string)-search_char_n_back+m_end+1]
    
    return string

def remove_empty_measurements(string):
    data_json = json.loads(string)
    meas_data = data_json['measurements']
    remove = []
    for m in meas_data:
        if len(meas_data[m])<1:
            remove.append(m)

    for r in remove:
        del data_json['measurements'][r]  

    return json.dumps(data_json, indent=4)

def clean_measurements():

    data_folder_rooms = Path("data/meas/")
    data_folder_rooms_json = Path("data/measjson/")
  #  filesToProcess= {"labm2_1","labm2_2","labm2_3","labm2_4","labm2_5","labm2_6","labm2_7","labm2_8","labm2_9","labm2_10","labm2_11","labm2_12","labm2_13","labm2_14","labm2_15"}
    #filesToProcess= ["labm3_A","labm3_B","labm3_C1","labm3_C2","labm3_D","labm3_E","labm3_F","labm3_G","labm3_H","labm3_I","labm3_J1","labm3_J2","labm3_K"]
    filesToProcess= ["mdir7"]
    mIdx = 0
  #  for file in filesToProcess:
    for i in range(len(filesToProcess)):
        #filenameIn = file + "meas.json"
        filenameIn = filesToProcess[i] + "meas.json"   

        file_measureAll = data_folder_rooms / filenameIn
        m_string = ms.MeasurementStrings()

        #read input file
        fin = open(file_measureAll, "rt")
        #read file contents to string
        data = fin.read()

        data, mIdx = replace_strings_mobile(data, mIdx)
            
        # count number of measures and split it to separate files
        measureS_split = data.split('{"measurements":')

        #close the input file
        fin.close()
        for mI in range(len(measureS_split)):
            if('"m' in measureS_split[mI]):
                string =proces_string_mobile(measureS_split[mI],m_string)

                #string_file =  "Out"+ file + str(mI) +'.json'
                #string_file =  "Out"+ filesToProcess[i] + str(mI) +'.json'
                string_file =  "Out"+ filesToProcess[i] + '_' + str(mI) +'.json'
                string_out = m_string.string_file_start + string + m_string.string_file_end
                string_out = remove_empty_measurements(string_out)
                file_mWjson = data_folder_rooms_json / string_file
                #open the input file in write mode
                fin = open(file_mWjson, "wt")
                #overrite the input file with the resulting data
                fin.write(string_out)
                #close the file
                fin.close()

def time_convert_utc(string):
    #string = string.replace('            ','')
    data_json = json.loads(string)
    meas_data = data_json['measurements']
    lineNumber = 0  
    lenData = len(meas_data)
    pctComplete = 0
    for m in meas_data:
     
        for mm in range(len(meas_data[m])):
            line = meas_data[m][mm]['TIME']
            
           
            if line != "":

                year = int(line[0:4])
                month = int(line[5:7])
                day = int(line[8:10])
                hour = int(line[11:13])
                minute = int(line[14:16])
                second = int(line[17:19])
                us = int(line[20:23])
                dt = datetime.datetime(year,month,day,hour,minute,second,us*1000)
                timOut = int(time.mktime(dt.timetuple())*1e3 + dt.microsecond/1e3)
            # print(dt)
                #out = out + str(timOut) + '\n'
                
                data_json['measurements'][m][mm]['TIME'] = timOut

        if lenData >1:
            if (lineNumber % int(lenData/10)) == 0:
                print('%d %% complete' % (pctComplete))
                pctComplete += 10
            lineNumber+=1
        else:
                print('%d %% complete' % (100))
    return json.dumps(data_json, indent=4)


def clean_measurements_wifi():

    data_folder_rooms = Path("data/meas/")
    data_folder_rooms_json = Path("data/measjson/")
    #filesToProcess= ["labm2_a","labm2_b","labm2_c","labm2_d","labm2_e","labm2_f","labm2_g","labm2_h","labm2_i","labm2_j","labm2_k","labm2_l","labm2_m","labm2_n","labm2_o"]
    #filesToProcess= ["labm3_A","labm3_B","labm3_C1","labm3_C2","labm3_D","labm3_E","labm3_F","labm3_G","labm3_H","labm3_I","labm3_J1","labm3_J2","labm3_K"]
    filesToProcess= ["mdir7"]

    for file in filesToProcess:
        filenameIn = file + ".json"

        file_measureAll = data_folder_rooms / filenameIn
        m_string = ms.MeasurementStrings()

        #read input file
        fin = open(file_measureAll, "rt")
        #read file contents to string
        data = fin.read()

        data = replace_strings_wifi(data)
            
        # count number of measures and split it to separate files
        measureS_split = data.split('\"measurements\":')

        #close the input file
        fin.close()
        search_char_n_back = 50
        for mI in range(len(measureS_split)):
            if('"m' in measureS_split[mI]):
                string = proces_string_wifi(measureS_split[mI],m_string)
                string_file =  "Out"+ file + str(mI) +'.json'

                string_out = m_string.string_file_start + string + m_string.string_file_end

                string_out = time_convert_utc(string_out)
                file_mWjson = data_folder_rooms_json / string_file
                #open the input file in write mode
                fin = open(file_mWjson, "wt")
                #overrite the input file with the resulting data
                fin.write(string_out)
                #close the file
                fin.close()


def merge_json():
    fileMobile = "mobile.json"
    fileWifi = "wifi.json"
    data_folder_merged = Path("data/mergedjson/")
    data_folder_rooms_json = Path("data/measjson/")

    from os import listdir
    from os.path import isfile, join
    onlyfiles = [f for f in listdir(data_folder_rooms_json) if isfile(join(data_folder_rooms_json, f))]
    onlyfiles.sort()
    #filesToProcessMobile= {"p1meas","p2b.measmeas","p3meas","p4correctmeas","p5correctmeas","p6meas","p7meas","p8meas"}
    #filesToProcess= {"p1measWifi","p2b.measmeasWifi","p3measWifi","p4correctmeasWifi","p5correctmeasWifi","p6measWifi","p7measWifi","p8measWifi"}
    measMobile = {}
    measWifi= {}

    for file in onlyfiles:

        if "Wifi" in file:
            fileWPath = data_folder_rooms_json / file
            fW = open(fileWPath)
            dataW = json.load(fW)
            measW = dataW['measurements']
            if len(measW)>0:
                measWifi.update(measW)

        else:

            fileMPath = data_folder_rooms_json / file
            fM = open(fileMPath, encoding='utf-8-sig')
            dataM = json.load(fM)
            measM = dataM['measurements']
            if len(measM)>0:
                measMobile.update(measM)

    outMobile = json.dumps(measMobile, indent=4)
    outWifi = json.dumps(measWifi, indent=4)

    file_mMjson = data_folder_merged / fileMobile
    file_mWjson = data_folder_merged / fileWifi

    #open the input file in write mode
    fin = open(file_mMjson, "wt")
    #overrite the input file with the resulting data
    fin.write(outMobile)
    #close the file
    fin.close()

    #open the input file in write mode
    fin = open(file_mWjson, "wt")
    #overrite the input file with the resulting data
    fin.write(outWifi)
    #close the file
    fin.close()

def merge_measurements_to_one():
    data_folder_merged = Path("data/mergedjson/")

    fileMobile = data_folder_merged / "mobile.json"
    fileMobileOut = data_folder_merged / "mobileO.json"

    fileWifi = data_folder_merged /"wifi.json"
    fileWifiOut = data_folder_merged /"wifiO.json"

    fin = open(fileMobile, "rt")
    #read file contents to string
    data = fin.read()
    fin.close()
    mIdx = 0

    counter = count(mIdx)   
  
    data = re.sub(r'm[0-9]+','m@',data)
    data = re.sub(r'@', lambda x: str(next(counter)), data )    

    #open the input file in write mode
    fin = open(fileMobileOut, "wt")
    #overrite the input file with the resulting data
    fin.write(data)
    #close the file
    fin.close()

    fin = open(fileWifi, "rt")
    #read file contents to string
    data = fin.read()
    fin.close()
    mIdx = 0

    counter = count(mIdx)   
  
    data = re.sub(r'm[0-9]+','m@',data)
    data = re.sub(r'@', lambda x: str(next(counter)), data )    

    #open the input file in write mode
    fin = open(fileWifiOut, "wt")
    #overrite the input file with the resulting data
    fin.write(data)
    #close the file
    fin.close()    

def get_mobile_wifi_header():
    data_folder_merged = Path("data/mergedjson/")
    data_folder_txt = Path("data/txt/")

    fileMobileIn = data_folder_merged / "mobileO.json"
    fileWifiIn = data_folder_merged /"wifiO.json"

    fileMobileOut = data_folder_txt / "uniqueMIdsO.txt"
    fileWifiOut = data_folder_txt / "uniqueWIdsO.txt"
    uniqueM = []
    uniqueW = []
    fMN = open(fileMobileIn)
    fWN = open(fileWifiIn)
    dataMN = json.load(fMN)
    dataWN = json.load(fWN)

    fin = open(fileWifiOut, "wt")
    for m in dataWN:
        for rec in dataWN[m]:
            if not rec['BSSID'] in uniqueW:
                uniqueW.append(rec['BSSID'])
                #overrite the input file with the resulting data
                fin.write(rec['BSSID'] + '\n')
    #close the file
    fin.close()

    #open the input file in write mode
    fin = open(fileMobileOut, "wt")
    for m in dataMN:
        for rec in dataMN[m]: # poradi 1,2,3,4,5,6,XX,XN,....,0,Wifi
            if not rec['PCI'] in uniqueM:
                uniqueM.append(rec['PCI'])
                #overrite the input file with the resulting data
                #fin.write(rec['PCI'] + '\n')   
                # sort uniqueM a potom zapsat v pozadovanem poradi
                for i in range(0, len(uniqueM)):
                    uniqueM[i] = int(uniqueM[i])
                uniqueM.sort()
                if uniqueM[0] == 0:
                    uniqueM.append(uniqueM.pop(0)) # circular shift left by 1
                for i in range(0, len(uniqueM)):
                    uniqueM[i] = str(uniqueM[i])
    for i in range(0, len(uniqueM)):
        fin.write(uniqueM[i] + '\n') 

    

    #close the file
    fin.close()


def get_average_sample_time():
    data_folder_txt = Path("data/txt/")
    data_folder_merged = Path("data/mergedjson/")

    file_mWTimes= data_folder_txt / "mWtimesO.txt"
    file_mMtimes = data_folder_txt / "mMtimesO.txt"


    fileMobileIn = data_folder_merged / "mobileO.json"
    fileWifiIn = data_folder_merged /"wifiO.json"

    fMN = open(fileMobileIn)
    fWN = open(fileWifiIn)
    dataMN = json.load(fMN)
    dataWN = json.load(fWN)


    measW = dataWN
    outTimesW = ''
    for m in measW:
        avgTime = 0
        numberOfSamples = 0
        for rec in measW[m]:
        # print("%s: %f" % (rec['SSID'], rec['TIME']))
            avgTime += int(rec['TIME'])
            #print("%s: %f" % (rec['SSID'], rec['TIME']))
            numberOfSamples+=1

        avgTime = int(avgTime/numberOfSamples)
        outTimesW = outTimesW + m + ':' + str(avgTime) + '\n'

    measM = dataMN

    outTimesM = ''
    for m in measM:
        avgTime = 0
        numberOfSamples = 0
        for rec in measM[m]:
        # print("%s: %f" % (rec['SSID'], rec['TIME']))
            avgTime += int(rec['time'])
            #print("%s: %f" % (rec['SSID'], rec['TIME']))
            numberOfSamples+=1

        avgTime = int(avgTime/numberOfSamples)
        outTimesM = outTimesM + m + ':' + str(avgTime) + '\n'    


    fMN.close()
    fWN.close()

    #open the input file in write mode
    fin = open(file_mWTimes, "wt")
    #overrite the input file with the resulting data
    fin.write(outTimesW)
    #close the file
    fin.close()

    #open the input file in write mode
    fin = open(file_mMtimes, "wt")
    #overrite the input file with the resulting data
    fin.write(outTimesM)
    #close the file
    fin.close()


def sample_mapping_time():
    data_folder_txt = Path("data/txt/")

    file_fMN= data_folder_txt / "mMtimesO.txt"
    file_fWN = data_folder_txt / "mWtimesO.txt"
    file_matching = data_folder_txt / "matchingO.txt"


    fMN = open(file_fMN)
    fWN = open(file_fWN)

    timesM = fMN.read()
    timesW = fWN.read()

    timesM = timesM.split('\n')
    timesW = timesW.split('\n')

    matchingList = ''

    indexSkip = -1
    for lineM in timesM:
        indexW = 0
        if lineM != '':
            recM = lineM.split(':')
        
            timeM = int(recM[1])
            
            for lineW in timesW:
                if indexW >=indexSkip:
                    recW = lineW.split(':')
                    if int(recW[1])>=timeM:
                        # check if previous or current time is closer
                        tmp = timesW[indexW-1].split(':')
                        dPrev = timeM - int(tmp[1])
                        dNext = int(recW[1]) - timeM

                        if dPrev < dNext:
                            match = tmp[0]
                        else:
                            match = recM[0]
                        indexSkip = indexW -1
                        break     
                    

                indexW += 1
            matchingList = matchingList + recM[0] + ':' + match + '\n'


    #open the input file in write mode
    fin = open(file_matching, "wt")
    #overrite the input file with the resulting data
    fin.write(matchingList)
    #close the file
    fin.close()  


def prepare_data_records():
    data_folder_txt = Path("data/txt/")
    data_folder_npy = Path("data/npy/")
    data_folder_merged = Path("data/mergedjson/")

    file_fmatch= data_folder_txt / "matchingO.txt"
    file_fMids = data_folder_txt / "uniqueMIdsO.txt"
    # file_fWids = data_folder_txt / "uniqueWIdsO.txt"

    file_fout= data_folder_txt / "mdir7.txt"
    file_foutNp= data_folder_npy / "dataNpO"

    file_fout_header= data_folder_txt / "dataHeaderInfo.txt"

    fileMobileIn = data_folder_merged / "mobileO.json"
    # fileWifiIn = data_folder_merged /"wifiO.json"

    fmatch = open(file_fmatch)
    # fW = open(fileWifiIn)
    fM = open(fileMobileIn) 

    fMids = open(file_fMids)
    # fWids = open(file_fWids)

    uMIds = fMids.read()
    # uWIds = fWids.read()

    uMIdsList = uMIds.split('\n')
    uMIdsList.pop()
    # uWIdsList = uWIds.split('\n')
    # uWIdsList.pop()

    nMids = uMIds.count('\n')
    # nWids = uWIds.count('\n')
    nIds =  nMids # + nWids

    idsListTemplate =  [0] * nIds
    

    # dataW = json.load(fW)
    dataM = json.load(fM)


    # measW = dataW
    measM = dataM

    matching = fmatch.read()
    nRecodrs = matching.count('\n')
    arr = np.empty([nRecodrs,nIds])
    matchRecord = matching.split('\n')
    matchRecord.pop()
    #open the input file in write mode
    fout = open(file_fout,"wt")
    
    dataOut = ''
    row = 0
    for match in matchRecord:
        indices = match.split(':')
        indexM = indices[0]
        # indexW = indices[1]

        for mM in measM:
            if mM == indexM:

                # for mW in measW:
                    # if mW == indexW:
                        idsList = idsListTemplate
                        # get cell measurements records and write to file
                        for rM in measM[mM]:
                            # find PCI in uMIdsList and add to idsList at correct location
                            position = uMIdsList.index(rM['PCI'])
                            idsList[position] = int(rM['RSPR'])
                            arr[row,position ] = idsList[position]
                      #  for rW in measW[mW]:
                            # find BSSID in uWIdsList and add to idsList at correct location
                            # find PCI in uMIdsList and add to idsList at correct location
                            # position = uWIdsList.index(rW['BSSID'])
                            # idsList[position+nMids] = int(rW['LEVEL'])
                            # arr[row,position+nMids] = idsList[position+nMids]

        writeIndex = 0 
        row +=1                 
        # write to file 
        for id in idsList:
            #overrite the input file with the resulting data
            fout.write(str(id))
            if writeIndex != (nIds-1):
                fout.write(',')
            else:
                fout.write('\n')  
            writeIndex += 1  

    #close the file
    fout.close()
    np.save(file_foutNp,arr)   

    foutH = open(file_fout_header,"wt")
    foutH.write("{cells:" + str(uMIdsList) +'},{count:' + str(len(uMIdsList))  + '} \n')
    # foutH.write("{wifi:" + str(uWIdsList) +',{count:' + str(len(uWIdsList))  + '} \n')
    foutH.close()

def create_headers():
    data_folder_txt = Path("data/txt/")

    file_fout_header= data_folder_txt / "dataHeaderInfo.txt"
    foutH = open(file_fout_header,"r")
    headers = foutH.read()
    headers = headers.split('\n')
    stringCount = 'count:'
    stringCountEnd = '}'

    idx = 0
    for h in headers:
        if len(h)>0:
            if idx == 0:
                nCell = int(h[h.index(stringCount)+len(stringCount):h.index(stringCountEnd,h.index(stringCount))])
            # else:
                # nWifi =int(h[h.index(stringCount)+len(stringCount):h.index(stringCountEnd,h.index(stringCount))])
            idx+=1
 
    cellStr = 'cell'
    # wifiStr = 'wifi'

    data_folder_txt = Path("data/txt/")


    file_output= data_folder_txt / "dataNpHeaderOM.txt"



    outString = ''
    separatingChar = ';'

    for i in range(nCell):
        outString += cellStr + str(i) + separatingChar

    #f or w in range(nWifi):
        # outString += wifiStr + str(w) + separatingChar

    #open the input file in write mode
    fin = open(file_output, "wt")
    #overrite the input file with the resulting data
    fin.write(outString)
    #close the file
    fin.close()   
    


prepareData = True
mergeData = True
correctMeasNumbering = True
getUniqueIds = True
getAverageTimes = True
sampleTimeMapping = False
prepareDataRecords = True
createHeaders = True


if prepareData:
    #prepare data
    clean_measurements()
    #clean_measurements_wifi()

if mergeData:
    # merge data splitted in multiple jsons to one
    merge_json()

if correctMeasNumbering:
    merge_measurements_to_one()


if getUniqueIds:
     get_mobile_wifi_header()

if getAverageTimes:
     get_average_sample_time()

if sampleTimeMapping:
     sample_mapping_time()     

if prepareDataRecords:
    prepare_data_records()  

if createHeaders:
    create_headers()   
