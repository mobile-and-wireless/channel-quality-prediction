from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely.geometry import MultiPolygon

point = Point(0.5, 0.5)
polygon1 = Polygon([(0, 0), (0, 1), (1, 1), (1, 0)])
polygon2 = Polygon([(0, 1), (0, 2), (1, 2), (1, 1)])
polygons = MultiPolygon([ polygon1, polygon2])

print(polygon1.contains(point))
print(polygons)
