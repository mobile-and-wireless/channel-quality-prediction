from scipy import interpolate
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib.ticker import LinearLocator

# define x axis properties
x_min = 0
x_max = 8
x_step = 1

# define y axis properties
y_max = 8
y_min = 0
y_step = 1

# define interpolation properties
x_step_interp = 0.1
y_step_interp = 0.1

# setup x and y points for interpolation
x = np.arange(x_min,x_max,x_step)
y = np.arange(y_min,y_max,y_step)

xx, yy = np.meshgrid(x, y)

# generate shadowing with given mu and sigma
mu, sigma = 0, 3 # mean and standard deviation
z = np.random.normal(mu, sigma, [x_max-x_min,y_max-y_min])

# interpolate
f = interpolate.interp2d(x, y, z, kind='cubic')

# determine shadowing values on interpolated grid (znew)
xnew = np.arange(x_min, x_max, x_step_interp)
ynew = np.arange(y_min, y_max, y_step_interp)

znew = f(xnew, ynew)


# Plot data
X, Y = np.meshgrid(xnew, ynew)

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
# Plot the surface.
surf = ax.plot_surface(X, Y, znew, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

# Customize the z axis.
ax.set_zlim(-sigma, sigma)
ax.zaxis.set_major_locator(LinearLocator(10))
# A StrMethodFormatter is used automatically
ax.zaxis.set_major_formatter('{x:.02f}')

# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()