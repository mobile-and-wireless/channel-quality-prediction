#!/bin/bash
ARGS=$(getopt -a --options pa:st --long "process,augment:,simulate,train" -- "$@")
eval set -- "$ARGS"

simulate=false
process=false
augment=false
train=false
while true; do 
    case "$1" in
        -p|--process)
            process=true
            shift;;
        -a|--augment)
            file_to_aug="$2"
            filename="${file_to_aug%.*}" # remove extension
            augment=true
            shift 2;;
        -s|--simulate)
            simulate=true     
            shift;;
        -t|--train)
            train=true
            shift;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

if $simulate; then
    echo "Running simulation..."
    python3 main.py # run simulation
    echo "Transfering data to mlmobile..."
    cp data/jsondepo/*.json ../mlmobile/data/jsonsim/ # copy simulated measurements into mlmobile repo
    cp data/txt/dataSim.txt ../mlmobile/data/txt/
    cp data/txt/dataSimint.txt ../mlmobile/data/txt/
fi

if $process; then
    cd ../mlmobile # jump to mlmobile repo
    echo "Processing measured data..."
    python3 processMobileOnly.py # proccess measured data
fi

if $augment; then
    if [ !$process ] ; then
        cd ../mlmobile
    fi
    echo "Augmenting $file_to_aug..."
    python3 dataAugmentation.py $file_to_aug  # augment. data
fi

if $train; then
    if [ !$process ]  &&  [ !$augment ]; then
        cd ../mlmobile
        echo "Preparing data for transfer learning"
        python3 train.py # prepare for transfer learning
        echo "Transfer learning in progress..."
        python3 transferLearning.py # transfer learning     
    fi

    if $augment; then
        file=$filename"_aug.txt"
        echo "Preparing data for transfer learning"
        python3 trainSelectedUpdateNew.py # prepare for transfer learning
        echo "Transfer learning in progress..."
        python3 transferLearning.py $file # transfer learning 
    fi
fi  
