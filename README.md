# Channel quality prediction


This repository is diveded into following sections:
- Android app ([cellinfo-master](cellinfo-master))
- Scripts for training neural network and processing dataset ([mlmobile-main](mlmobile))
- Scripts for digital environment ([ml-mobile-simulation-main](ml-mobile-simulation-main))
- Scripts used with software-define mobile network ([mlserver-main](mlserver-main))

## Library dependecies
- scipy == 1.13.1
- numpy == 1.26.4
- tensorflow (keras) == 2.14
- seaborn == 0.13.2
- pandas == 2.2.3

## Brief tutorial

To achieve data from simulation, run [main.py](ml-mobile-simulation-main/main.py) which is preparing digital twin of 5G/6G mobile testbed. Afterwards, it saves obtained data from the digital environment. 
In following step, the training of the DNN using data from digital environment is proccess in [train.py](mlmobile/train.py) and save trained basic model [modelBasic.tf](mlmobile/data/mdl/modelBasic.tf). The transfer learning uses real data to improve already trained model (modelBasic.tf) with data from digital twin. The transfer learning is implemented in [transferLearning.py](mlmobile-main/transferLearning.py). Model trained by tranfer learning is saved as [modelTransfer.tf](mlmobile/data/mdl/modelTransfer.tf).

Usage of DNN with software defined mobile network: on the gNB computer, run [server.py](mlserver-main/server.py) establishing TCP connection with UE. The server communicates with UE, collects data for the subsequent training or prediction. On the UE side, run [client.py](mlserver-main/client.py) or [CellInfo](cellinfo-master) app. To predict channel prediction using trained model, run [predictData.py](mlmobile/predictData.py).

Most of the data is stored in [this folder](mlmobile/data/txt)

## How to run code

```bash
#run simulation
cd ml-mobile-simulation-main
./ runSimulation.sh --simulate

#augment data
cd ml-mobile-simulation-main
./ runSimulation.sh --augment <file> #required *int.txt file

#train model
cd mlmobile
python3 train.py
python3 transferLearning.py

#collect data
cd mlserver-main
python3 server.py #first terminal
python3 client.py #second terminal

#predict data
cd mlmobile
python3 predictData.py
```

## Acknowledgement

In case you use he CellInfo app or the dataset for channel quality prediction, please cite the paper:

Z. Becvar, J. Plachy, P. Mach, A. Nikolov and D. Gesbert, "Machine Learning for Channel Quality Prediction: From Concept to Experimental Validation," IEEE Transactions on Wireless Communications, Early access, June 2024. 
https://ieeexplore.ieee.org/document/10577598
