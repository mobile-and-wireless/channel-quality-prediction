import socket
import struct


def Main():
    # host = '192.168.27.111'
    # port = 1236

    host = '127.0.0.1'
    port = 1234

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    message = "HELLO"  # input("->")
    myint = 0
    send_msg(s, message)

    # s.send(message.encode('utf-8'))
    data = recv_msg(s).decode('utf-8')
    # data = s.recv(1024).decode('utf-8')
    if (data.__contains__('210 Hello')):
        print("Connection  established, input your command (COLLECT, BYE)")
        while True:
            input1 = input()
            if (input1.__contains__("COLLECT")):
                print("collect switch")
                processData(s)
            elif (input1.__contains__("BYE")):
                print("collect bye")
                message = ("BYE")
                # s.send(message.encode('utf-8'))
                send_msg(s, message)
                data = recv_msg(s)
                data = data.decode('utf-8')
                # data = s.recv(1024).decode('utf-8')
                print(data)
                s.close()
    else:
        print("Connection not established")
        s.close()


def processData(s):
    # while True:
    message = ("COLLECT")
    send_msg(s, message)
    data = recv_msg(s)
    data = data.decode('utf-8')
    # s.send(message.encode('utf-8'))
    # data = s.recv(1024).decode('utf-8')
    print(data)
    if (data.__contains__('BYE')):
        print("Connection terminated")
        s.close()


def send_msg(sock, msg):
    # Prefix each message with a 4-byte length (network byte order)
    msg = struct.pack('>I', len(msg)) + msg.encode('UTF-8')
    sock.sendall(msg)


def recv_msg(sock):
    # Read message length and unpack it into an integer
    raw_msglen = recvall(sock, 4)
    if not raw_msglen:
        return None
    msglen = struct.unpack('>I', raw_msglen)[0]
    # Read the message data
    return recvall(sock, msglen)


def recvall(sock, n):
    # Helper function to recv n bytes or return None if EOF is hit
    data = bytearray()
    while len(data) < n:
        packet = sock.recv(n - len(data))
        if not packet:
            return None
        data.extend(packet)
    return data


if __name__ == '__main__':
    Main()
