import requests
import json
import os
import getopt
import influxdb_client 
from influxdb_client.client.write_api import SYNCHRONOUS
import time

bucket = "flex"
org = "CTU"
token = "mWVjyv9wfrxKqs0wD18mpHd9VQ27W6v3F7SBdoWZpamXRrvPCPxHVgXtiQwOBYxhjwV3GPwRFBKChC5RQOJSuw=="
# Store the URL of your InfluxDB instance
url="http://192.168.25.11:8086"

print('Connecting to InfluxDB at:', url, "for org:", org, "and storing data to bucket:", bucket)
print('The serving cell has negative value, e.g. -1')

client = influxdb_client.InfluxDBClient(
  url=url,
  token=token,
  org=org
)

measuringBool = False

write_api = client.write_api(write_options=SYNCHRONOUS)

while True:

    cellId1 = "Cell Id: 31"
    cellId2 = "Cell Id: 32"
    cellId3 = "Cell Id: 33" 
    foundCellId1 = False
    foundCellId2 = False
    foundCellId3 = False

    # Check and retrieve command-line arguments
    #if len(sys.argv) != 2:
    #   print(__doc__)
    #  sys.exit(1)   # Return a non-zero value to indicate abnormal termination

    fileIn  = "/tmp/test.txt" #sys.argv[1]
    lineNumberStart = 0 #int(sys.argv[2])
    if os.path.exists(fileIn):
        os.remove(fileIn)
    else:
        print("Can not delete the file as it doesn't exists, yet")

    print("Waiting for file to be created") 

    while not os.path.exists(fileIn):
        time.sleep(1) # Sleep for 10 seconds


    print("File has been created, staring check") 

        # Verify source file
    if not os.path.isfile(fileIn):
        print("error: {} does not exist".format(fileIn))
        sys.exit(1)

    lineNumber = 0

    while True:
        # Process the file line-by-line
        fpIn = open(fileIn,'r')
        Lines = fpIn.readlines()
        print("Collecting RSRP")
        for line in Lines:
            lineNumber += 1
                
            if lineNumber>lineNumberStart:
                if line.find("Serving")>0:
                    Meas = line[line.find("Serving"):]
                    mylist = Meas.split("; ")
                    servingbool = True
                    cellIdBool = True
                    for measStr in mylist:
                        if measStr.find('RSRP')>0:
                            cellIdTmp =  measStr.split(", ") 
                            for cellI in cellIdTmp:
                                if cellIdBool:
                                    if servingbool:
                                        cellId =  int(cellI.replace('Serving Cell Id: ',''))
                                        servingbool = False
                                    else:
                                        cellId =  int(cellI.replace('Cell Id: ',''))

                                    cellIdBool = False
                                else:
                                    if cellId == 0:
                                        servingRsrp = int(cellI.replace('RSRP: ','').replace(';',''))

                                        servingP = influxdb_client.Point("my_measurement3").tag("cellId", 0).field("rsrp", servingRsrp)
                                        write_api.write(bucket=bucket, org=org, record=servingP)
                                    else:
                                        cellRsrp = int(cellI.replace('RSRP: ','').replace(';',''))

                                        p = influxdb_client.Point("my_measurement3").tag("cellId", cellId).field("rsrp", cellRsrp)
                                        write_api.write(bucket=bucket, org=org, record=p)

                                    cellIdBool = True
        fpIn.close()
        lineNumberStart = lineNumber
        print("Collecting RSRP") 
        time.sleep(0.01)
