def build_and_compile_model(norm):
    model = keras.Sequential([
        norm,
        layers.Dense(20, activation='relu'),
        layers.Dense(18, activation='relu'),
        layers.Dense(15, activation='relu'),
        layers.Dense(12, activation='relu'),
        layers.Dense(12, activation='relu'),
        layers.Dense(1)
    ])

    model.compile(loss='mean_absolute_error',
                optimizer=tf.keras.optimizers.Adam(0.001))
    return model


def build_and_compile_modelM(norm):
    model = keras.Sequential([
        norm,
        layers.Dense(20, activation='relu'),
        layers.Dense(18, activation='relu'),
        layers.Dense(15, activation='relu'),
        layers.Dense(12, activation='relu'),
        layers.Dense(8, activation='relu'),
        layers.Dense(1)
    ])

    model.compile(loss='mean_absolute_error',
                optimizer=tf.keras.optimizers.Adam(0.001))
    return model

def train_dnn():
    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    import seaborn as sns
    from numpy import loadtxt

    import tensorflow as tf
    from tensorflow.keras import layers
    from tensorflow import keras


    # Make numpy printouts easier to read.
    np.set_printoptions(precision=3, suppress=True)

    dataset = loadtxt('data.txt', delimiter=',')
    column_names = ['31', '32', '33', '11']

    raw_dataset = pd.read_csv('data.txt', names=column_names,
                            na_values='?', comment='\t',
                            sep=',', skipinitialspace=True)

    dataset = raw_dataset.copy()
    dataset.tail()
    dataset.isna().sum()

    train_dataset = dataset.sample(frac=0.8, random_state=0)
    test_dataset = dataset.drop(train_dataset.index)
    train_dataset.describe().transpose()

    train_features = train_dataset.copy()
    test_features = test_dataset.copy()

    train_labels = train_features.pop('11')
    test_labels = test_features.pop('11')

    normalizer =  tf.keras.layers.Normalization(axis=-1)
    normalizer.adapt(np.array(train_features))
    cell1 = np.array(train_features)
    print(cell1[:10])
    cell1_normalizer = tf.keras.layers.Normalization(input_shape=[3,], axis=None)
    cell1_normalizer.adapt(cell1)

    dnn_cell_model = build_and_compile_modelM(cell1_normalizer)

    dnn_cell_model.summary()
    history = dnn_cell_model.fit(
        train_features, train_labels,
        validation_split=0.2,
        verbose=0, epochs=100)

    #x = tf.linspace(-100, -75, 25)


def predict(x):    
    y = dnn_cell_model.predict(cell1[:10])
    print(dnn_cell_model.predict(cell1[:10]))