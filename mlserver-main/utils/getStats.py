import requests
import json
import influxdb_client 
from influxdb_client.client.write_api import SYNCHRONOUS
import time

bucket = "flex"
org = "CTU"
token = "mWVjyv9wfrxKqs0wD18mpHd9VQ27W6v3F7SBdoWZpamXRrvPCPxHVgXtiQwOBYxhjwV3GPwRFBKChC5RQOJSuw=="
# Store the URL of your InfluxDB instance
url="http://127.0.1.1:8086"

print('Connecting to InfluxDB at:', url, "for org:", org, "and storing data to bucket:", bucket)
print('The serving cell has negative value, e.g. -1')

client = influxdb_client.InfluxDBClient(
  url=url,
  token=token,
  org=org
)

measuringBool = False

write_api = client.write_api(write_options=SYNCHRONOUS)

while True:
  r = requests.get('http://127.0.1.1:9999/stats/mac_stats')
  flexJson = r.json()
  if 'mac_stats' in flexJson :

    rrcMeas = flexJson['mac_stats'][0]['ue_mac_stats'][0]['mac_stats']
    if 'rrcMeasurements' in rrcMeas :

      serving = flexJson['mac_stats'][0]['ue_mac_stats'][0]['mac_stats']['rrcMeasurements']['measid']

      if (serving != -1):
        servingRsrp = flexJson['mac_stats'][0]['ue_mac_stats'][0]['mac_stats']['rrcMeasurements']['pcellRsrp']
        servingRsrq = flexJson['mac_stats'][0]['ue_mac_stats'][0]['mac_stats']['rrcMeasurements']['pcellRsrq']
        servingP = influxdb_client.Point("my_measurement").tag("cellId", 0).field("rsrp", servingRsrp).field("rsrq", servingRsrq)
        write_api.write(bucket=bucket, org=org, record=servingP)
    
        neigh = flexJson['mac_stats'][0]['ue_mac_stats'][0]['mac_stats']['rrcMeasurements']
        if 'neighMeas' in neigh :

          if not measuringBool:
            print('Collecting UE neighbor cell list')
            measuringBool = True

          measurement =flexJson['mac_stats'][0]['ue_mac_stats'][0]['mac_stats']['rrcMeasurements']['neighMeas']['eutraMeas']
          for x in measurement:
            cellId = x['physCellId']
            cellRsrp = x['measResult']['rsrp']
            cellRsrq = x['measResult']['rsrq']
            p = influxdb_client.Point("my_measurement").tag("cellId", cellId).field("rsrp", cellRsrp).field("rsrq", cellRsrq)
            write_api.write(bucket=bucket, org=org, record=p)
        else:
          print('no neighboring cells, waiting for 5 seconds')
          measuringBool = False
          time.sleep(5)

        time.sleep(0.01)
      else:
        print('UE is not connected, restart application after the UE connects to the network')
        break
    else:
      print('No measurement data available, restart application in a couple of seconds')
      break

    
