#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys
import os
import getopt
#from shutil import copyfile
from datetime import datetime


def main(argv):

    room = 'n'
    location = 'l'
    try:
        opts, args = getopt.getopt(argv,"hr:l:",["room=","location="])
    except getopt.GetoptError:
        print("test.py -r <room> -l <location>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("test.py -r <room> -l <location>")
            sys.exit()
        elif opt in ("-r", "--room"):
            room = arg
        elif opt in ("-l", "--location"):
            location = arg
    print ("Room is " + room)
    print ("Location file is " + location)
    stringRoomLocation = "_room_" + room + "_location_" + location
    print(stringRoomLocation)
    check(stringRoomLocation) 
 
def check(stringRoomLocation):
    import time
    cellId1 = "Cell Id: 31"
    cellId2 = "Cell Id: 32"
    cellId3 = "Cell Id: 33" 
    foundCellId1 = False
    foundCellId2 = False
    foundCellId3 = False

    # Check and retrieve command-line arguments
    #if len(sys.argv) != 2:
    #   print(__doc__)
    #  sys.exit(1)   # Return a non-zero value to indicate abnormal termination

    fileIn  = "/tmp/test.txt" #sys.argv[1]
    lineNumberStart = 0 #int(sys.argv[2])
    if os.path.exists(fileIn):
        os.remove(fileIn)
    else:
        print("Can not delete the file as it doesn't exists, yet")

    print("Waiting for file to be created") 

    while not os.path.exists(fileIn):
        time.sleep(10) # Sleep for 10 seconds

    print("File has been created, staring check") 

        # Verify source file
    if not os.path.isfile(fileIn):
        print("error: {} does not exist".format(fileIn))
        sys.exit(1)

    lineNumber = 0

    while True:
        # Process the file line-by-line
        fpIn = open(fileIn,'r')
        Lines = fpIn.readlines()

        for line in Lines:
            lineNumber += 1
                
            if lineNumber>lineNumberStart:
                if line.find(cellId1)>0:
                    foundCellId1 = True

                if line.find(cellId2)>0:
                    foundCellId2 = True
                
                if line.find(cellId3)>0:
                    foundCellId3 = True

        fpIn.close()
        missing = ""
        if(foundCellId1 and foundCellId2 and foundCellId3):
            print("Found all neighboring cells")
            print("Waiting 5 more seconds to colect all data")
            time.sleep(5) # Sleep for 10 seconds
            now = datetime.now()
            time = now.strftime("%H_%M_%S")
            if not stringRoomLocation:
                os.rename(fileIn, "/tmp/meas"  + time + ".txt")
            else:
                os.rename(fileIn, "/tmp/meas" + stringRoomLocation + "_time_" + time + ".txt")

            print("Done collecting RSRP")
            break
        else:
            lineNumberStart = lineNumber
            if foundCellId1==False:
                missing = missing + "31" + ", "
                #print("Missing 31")
            if foundCellId2==False:
                missing = missing + "32" + ", "
                #print("Missing 32")

            if foundCellId3==False:
                missing = missing + "33" + " "
                #print("Missing 33")

            print("Missing neighboring cells: " + missing + " sleep for 10 s")
            time.sleep(10) # Sleep for 10 seconds
     

if __name__ == "__main__":
    
    main(sys.argv[1:]) 